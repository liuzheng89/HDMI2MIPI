﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;

using System.Reflection;

namespace WindowsFormsApp1
{
    public partial class MainFrm : Form
    {
        public MainFrm()
        {
            InitializeComponent();
        }

        private void btnIcTuning_Click(object sender, EventArgs e)
        {

        }

        private void Dis_mode_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void chkDEPos_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void LCD_Cfg_Click(object sender, EventArgs e)
        {

        }

        private void chkclkpos_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkVsync_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkHsync_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnRun_Click(object sender, EventArgs e)
        {

        }

        private void SSD28xx_Download_Click(object sender, EventArgs e)
        {

        }

        private void btnMIPIDriverW_Click(object sender, EventArgs e)
        {

        }

        private void txtMIPIDriver_TextChanged(object sender, EventArgs e)
        {

        }

        private void lstMIPIDriver_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void lstMIPIDriver_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void btnOpenCom_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void SSD28xx_Download_only_Click(object sender, EventArgs e)
        {

        }

        private void btnMipiSSDW_Click(object sender, EventArgs e)
        {

        }

        private void txtMIPISSD_TextChanged(object sender, EventArgs e)
        {

        }

        private void lsltMIPISSD_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void lstMIPISSD_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            StreamReader sr = File.OpenText(@"C:\Users\hufz\Desktop\DSC\DSC_model_20161212\DSC_model_20161212\DSC_model_20161212\rc_14bpc_4bpp_420.cfg");
            string nextLine;
            while ((nextLine = sr.ReadLine()) != null)
            {
                //Console.WriteLine(nextLine);
                if (nextLine.Contains("//"))        //移除注释
                {
                    int i = nextLine.IndexOf("//");
                    string str = nextLine.Substring(i, nextLine.Length - i);
                    nextLine = nextLine.Replace(str, "");
                }  
                if (nextLine.ToLower().Contains("BITS_PER_COMPONENT".ToLower()))
                {                    
                    nextLine = nextLine.Replace("BITS_PER_COMPONENT", "");
                    nextLine = nextLine.Trim();
                    cmbxBITS_PER_COMPONENT.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("BITS_PER_PIXEL".ToLower()))
                {
                    nextLine = nextLine.Replace("BITS_PER_PIXEL", "");
                    nextLine = nextLine.Trim();
                    cmbxBITS_PER_PIXEL.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_MODEL_SIZE".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_MODEL_SIZE", "");
                    nextLine = nextLine.Trim();
                    txtRC_MODEL_SIZE.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_BUF_THRESH".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_BUF_THRESH", "");
                    nextLine = nextLine.Trim();
                    txtRC_BUF_THRESH.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_MINQP".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_MINQP", "");
                    nextLine = nextLine.Trim();
                    txtRC_MINQP.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_MAXQP".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_MAXQP", "");
                    nextLine = nextLine.Trim();
                    txtRC_MAXQP.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_OFFSET".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_OFFSET", "");
                    nextLine = nextLine.Trim();
                    txtRC_OFFSET.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_TGT_OFFSET_HI".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_TGT_OFFSET_HI", "");
                    nextLine = nextLine.Trim();
                    txtRC_TGT_OFFSET_HI.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_TGT_OFFSET_LO".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_TGT_OFFSET_LO", "");
                    nextLine = nextLine.Trim();
                    txtRC_TGT_OFFSET_LO.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_EDGE_FACTOR".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_EDGE_FACTOR", "");
                    nextLine = nextLine.Trim();
                    txtRC_EDGE_FACTOR.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_QUANT_INCR_LIMIT0".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_QUANT_INCR_LIMIT0", "");
                    nextLine = nextLine.Trim();
                    txtRC_QUANT_INCR_LIMIT0.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("RC_QUANT_INCR_LIMIT1".ToLower()))
                {
                    nextLine = nextLine.Replace("RC_QUANT_INCR_LIMIT1", "");
                    nextLine = nextLine.Trim();
                    txtRC_QUANT_INCR_LIMIT1.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("INITIAL_FULLNESS_OFFSET".ToLower()))
                {
                    nextLine = nextLine.Replace("INITIAL_FULLNESS_OFFSET", "");
                    nextLine = nextLine.Trim();
                    txtINITIAL_FULLNESS_OFFSET.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("INITIAL_DELAY".ToLower()))
                {
                    nextLine = nextLine.Replace("INITIAL_DELAY", "");
                    nextLine = nextLine.Trim();
                    txtINITIAL_DELAY.Text = nextLine;
                }
                //else if (nextLine.ToLower().Contains("SECOND_LINE_BPG_OFFSET".ToLower()))
                //{
                //    nextLine = nextLine.Replace("SECOND_LINE_BPG_OFFSET", "");
                //    nextLine = nextLine.Trim();
                //    txtSECOND_LINE_BPG_OFFSET.Text = nextLine;
                //}
                //else if (nextLine.ToLower().Contains("SECOND_LINE_OFFSET_ADJ".ToLower()))
                //{
                //    nextLine = nextLine.Replace("SECOND_LINE_OFFSET_ADJ", "");
                //    nextLine = nextLine.Trim();
                //    txtSECOND_LINE_OFFSET_ADJ.Text = nextLine;
                //}
                else if (nextLine.ToLower().Contains("FLATNESS_MIN_QP".ToLower()))
                {
                    nextLine = nextLine.Replace("FLATNESS_MIN_QP", "");
                    nextLine = nextLine.Trim();
                    txtFLATNESS_MIN_QP.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("FLATNESS_MAX_QP".ToLower()))
                {
                    nextLine = nextLine.Replace("FLATNESS_MAX_QP", "");
                    nextLine = nextLine.Trim();
                    txtFLATNESS_MAX_QP.Text = nextLine;
                }
                else if (nextLine.ToLower().Contains("FLATNESS_DET_THRESH".ToLower()))
                {
                    nextLine = nextLine.Replace("FLATNESS_DET_THRESH", "");
                    nextLine = nextLine.Trim();
                    txtFLATNESS_DET_THRESH.Text = nextLine;
                }
                //else if (nextLine.ToLower().Contains("".ToLower()))
                //{
                //    nextLine = nextLine.Replace("", "");
                //    nextLine = nextLine.Trim();
                //    txt.Text = nextLine;
                //}
            }
            sr.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string strLine = "", strLine1 = "";
            byte[] TemFileContent = new byte[4096];
            System.Int32 ByteReaded = 0x00;
            byte data;

            int length = 0;
            int code_length = 0;

            OpenFileDialog of = new OpenFileDialog();
            string[] arr = new string[12];
            int i = 0;
            of.Filter = @"cfg文件(*.cfg)|*.cfg|所有文件(*.*)|*.*"; ;
            of.Title = "载入LCD配置文件";

            if (of.ShowDialog() == DialogResult.OK)
            {
                //lstMIPISSD.Items.Clear();
                //lstMIPIDriver.Items.Clear();
                //读取文本信息
                FileStream fs = new FileStream(of.FileName, FileMode.Open, FileAccess.Read);
                fs.Seek(0, SeekOrigin.Begin);
                ByteReaded = fs.Read(TemFileContent, 0, 64);//读取文件从地址0的512个数据
                byte[] t = new byte["GVOConfi1.CFG".Length];
                Array.Copy(TemFileContent, t, "GVOConfi1.CFG".Length);
                string tt = Encoding.UTF8.GetString(t, 0, "GVOConfi1.CFG".Length);
                Console.WriteLine(tt);

                if (true == String.Equals(tt, "GVOConfi1.CFG"))
                {

                    for (i = 0; i < 32; i++)
                    {
                        Program.LCDParameters[i] = TemFileContent[0x20 + i];
                    }
                    //signal_mode = Program.LCDParameters[31];
                    HACT.Value = (Program.LCDParameters[1] * 256 + Program.LCDParameters[0]);
                    HBP.Value = (Program.LCDParameters[5] * 256 + Program.LCDParameters[4]);
                    HFP.Value = (Program.LCDParameters[9] * 256 + Program.LCDParameters[8]);
                    HSW.Value = (Program.LCDParameters[13] * 256 + Program.LCDParameters[12]);
                    VACT.Value = (Program.LCDParameters[3] * 256 + Program.LCDParameters[2]);
                    VBP.Value = (Program.LCDParameters[7] * 256 + Program.LCDParameters[6]);
                    VFP.Value = (Program.LCDParameters[11] * 256 + Program.LCDParameters[10]);
                    VSW.Value = (Program.LCDParameters[15] * 256 + Program.LCDParameters[14]);
                    Pixel_Clock.Value = (((decimal)(Program.LCDParameters[17] + Program.LCDParameters[18] * 256) / 10));
                    Pixel_Clock.Enabled = true;
                    txtLCD_CFG.Text = (Program.LCDParameters[24]).ToString();
                    data = Program.LCDParameters[16];
                    //Sig_mode.SelectedIndex = ((data) & 0x0f);
                    if ((data) >> 4 == 1)
                    { LabMOdelSelect.Text = "VIDEO MODE"; }
                    else { LabMOdelSelect.Text = "COMMAND MODE"; }

                    //if (Dis_mode.SelectedIndex == 1)
                    //{
                    //Pixel_Clock.Value = (((decimal)(Program.LCDParameters[17] + Program.LCDParameters[18] * 256) / 10));
                    //Pixel_Clock.Enabled = true;
                    //}
                    //else
                    //{
                    //    Pixel_Clock.Value = (decimal)(120.0);
                    //    Pixel_Clock.Enabled = false;
                    //}
                    chkclkpos.Checked = Convert.ToBoolean((int.Parse(Program.LCDParameters[24].ToString()) >> 0) & 0x01);
                    chkDEPos.Checked = Convert.ToBoolean((int.Parse(Program.LCDParameters[24].ToString()) >> 1) & 0x01);
                    chkHsync.Checked = Convert.ToBoolean((int.Parse(Program.LCDParameters[24].ToString()) >> 2) & 0x01);
                    chkVsync.Checked = Convert.ToBoolean((int.Parse(Program.LCDParameters[24].ToString()) >> 3) & 0x01);

                    //if (signal_mode >= 0xa0)
                    //{

                    //    IC_comboBox.SelectedIndex = signal_mode - 0xa0;

                    //    SSD_group.Enabled = true;
                    //    ID_driver_group.Enabled = true;



                    //    fs.Seek(0x40, SeekOrigin.Begin);            //直接跳到文件的0x3F的位置
                    //    ByteReaded = fs.Read(TemFileContent, 0, 512);//读取文件从地址0的512个数据
                    //    for (i = 0; i < 512; i++)
                    //    {
                    //        if (TemFileContent[i * 3] == 0xff)
                    //        {
                    //            length++;
                    //            break;
                    //        }
                    //        strLine = TemFileContent[i * 3].ToString("x2") + "=" + TemFileContent[i * 3 + 1].ToString("x2") + TemFileContent[i * 3 + 2].ToString("x2");
                    //        lstMIPISSD.Items.Add(strLine);
                    //        length += 3;
                    //    }
                    //    lstMIPISSD.SelectedIndex = 0;
                    //    txtMIPISSD.Text = Convert.ToString(lstMIPISSD.SelectedItem);
                    //}
                    //else if (signal_mode == 0x55)
                    //{
                    //    IC_comboBox.SelectedIndex = 0x02;
                    //    SSD_group.Enabled = true;
                    //    ID_driver_group.Enabled = true;
                    //}
                    //fs.Seek(0x40 + length, SeekOrigin.Begin);            //直接跳到文件的0x41+2828配置数据长度的位置 
                    //ByteReaded = fs.Read(TemFileContent, 0, 4096);//读取文件从地址0的512个数据
                    //fs.Close();

                    //for (i = 0; i < 4096; i += (code_length + 1))
                    //{
                    //    strLine1 = "";
                    //    code_length = TemFileContent[i];

                    //    if (code_length == 0xff)
                    //    {
                    //        break;
                    //    }

                    //    for (int j = 1; j < code_length + 1; j++)
                    //    {
                    //        if (j == code_length)
                    //            strLine1 = strLine1 + TemFileContent[i + j].ToString("x2");
                    //        else
                    //            strLine1 = strLine1 + TemFileContent[i + j].ToString("x2") + ",";
                    //    }
                    //    lstMIPIDriver.Items.Add(strLine1);

                    //}
                    //lstMIPIDriver.SelectedIndex = 0;
                    //txtMIPIDriver.Text = Convert.ToString(lstMIPIDriver.SelectedItem);
                    //btnReload.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Not a invalid config file!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //return;
                }
            }
        }
    }
}
