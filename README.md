# HDMI2MIPI

#### 项目介绍
利用LT6911UXC 把HDMI转成MIPI信号。   
The LT6911UXC is a high performance HDMI2.0 to MIPI   
DSI/CSI converter.   
The HDMI2.0 input supports data rate up to 6Gbps which provides sufficient bandwidth for 4k@60Hz video. Also HDCP2.2 is supported for data decryption.   
HDMI2.0 Receiver   
 Compliant with HDMI2.0b, HDMI1.4 and DVI1.0   
 Compliant with HDCP2.2 and HDCP1.4   
 Data rate up to 6Gbps   
 Adaptive receiver equalization   
 AC-couple capable   
 Support channel swap       

#### 平台架构
    USB3300+STM32+LT6911

stm32与LT6911通过硬件IIC连接，LT6911的PIN26用作串口的RX.
