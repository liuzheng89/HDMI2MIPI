/**
  ******************************************************************************
  * @file    usbd_cdc_core.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    22-July-2011
  * @brief   This file provides the high layer firmware functions to manage the 
  *          following functionalities of the USB CDC Class:
  *           - Initialization and Configuration of high and low layer
  *           - Enumeration as CDC Device (and enumeration for each implemented memory interface)
  *           - OUT/IN data transfer
  *           - Command IN transfer (class requests management)
  *           - Error management
  *           
  *  @verbatim
  *      
  *          ===================================================================      
  *                                CDC Class Driver Description
  *          =================================================================== 
  *           This driver manages the "Universal Serial Bus Class Definitions for Communications Devices
  *           Revision 1.2 November 16, 2007" and the sub-protocol specification of "Universal Serial Bus 
  *           Communications Class Subclass Specification for PSTN Devices Revision 1.2 February 9, 2007"
  *           This driver implements the following aspects of the specification:
  *             - Device descriptor management
  *             - Configuration descriptor management
  *             - Enumeration as CDC device with 2 data endpoints (IN and OUT) and 1 command endpoint (IN)
  *             - Requests management (as described in section 6.2 in specification)
  *             - Abstract Control Model compliant
  *             - Union Functional collection (using 1 IN endpoint for control)
  *             - Data interface class

  *           @note
  *             For the Abstract Control Model, this core allows only transmitting the requests to
  *             lower layer dispatcher (ie. usbd_cdc_vcp.c/.h) which should manage each request and
  *             perform relative actions.
  * 
  *           These aspects may be enriched or modified for a specific user application.
  *          
  *            This driver doesn't implement the following aspects of the specification 
  *            (but it is possible to manage these features with some modifications on this driver):
  *             - Any class-specific aspect relative to communication classes should be managed by user application.
  *             - All communication classes other than PSTN are not managed
  *      
  *  @endverbatim
  *                                  
  ******************************************************************************               
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "usbd_cdc_core.h"
#include "usbd_desc.h"
#include "usbd_req.h"
#include "main.h"


/** @addtogroup STM32_USB_OTG_DEVICE_LIBRARY
  * @{
  */


/** @defgroup usbd_cdc 
  * @brief usbd core module
  * @{
  */ 

/** @defgroup usbd_cdc_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup usbd_cdc_Private_Defines
  * @{
  */ 
/**
  * @}
  */ 

u8 NUM=0;
u8 Valid_BMP_No = 0;
OLED_Para OLED;
u32 WriteAddress = Inter_Flash_Addr;
u32 buffer1[BMP_R_Byte_Max/2];
u8	Uart_rx_flag = REC_W_LCDCONF_FLAG;
u8  buffer[BMP_R_Byte_Max];
u32 i = 0,j = 0,k = 0;
u32 File_length=0;
u8 SendTo_PC_cfgFile=0;
u32 VCP_Receive_True_num=0;
u32 ICS307_Valid_Data=0;
u32 ICS307_Buf_Data=0;
u32 ICS307_Buf_Data1 = 0;
u8 PatternR=0,PatternG=0,PatternB=0;
u8 Pure_Color_Flag = 0;
u8 CS_Master = CS1;
u8 CS_Slaver = CS2;
u16 LP_B7_Data,HS_B7_Data;
u8  Rec_first_line_Flag = 1;
u8 RM195_otptime = 0;
u32 SSD28xx_Code[SSD2828_Code_Max];                         //存放 SSD2828配置 数据
u32 Timing_and_InitCode[IniCode_Max];                       //更新时 临时存放Timing和Driver IC初始化代码
u8  SSD28xxCode_count = 0;                                  //SSD2828配置代码的 行数
u8  InitCode_valid[IniCode_table_num][IniCode_Max];         //存放 Driver IC初始化代码
u8  InitCode_count = 0;                                     //DriverIC初始化代码的 行数
u32 Code_length = 0;
u8  Receive_Code_flag = 0;
//----------------------------------------------------------------------------------
/*RM67195 Gamma寄存器位址*/
u8 RM195_Gamma_r[62]=
{       
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
    0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
    0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2D,0x2F,0x30,0x31,
    0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3D,0x3F,0x40,0x41
};
u8 RM195_Gamma_g[62]=
{
    0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,
    0x52,0x53,0x54,0x55,0x56,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,
    0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,
    0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80
};
u8 RM195_Gamma_b[62]=
{
    0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,
    0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,
    0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0xB2,
    0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,0xC0
};

//----------------------------------------------------------------------------------
/*RM67295 Gamma寄存器位址*/
u8 RM295_Gamma_r[62]=
{       
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
    0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
    0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x30,0x31,0x32,0x33,
		0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x40,0x41,0x42,0x43,0x44,0x45
};
u8 RM295_Gamma_g[62]=
{
    0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,0x58,0x59,
		0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,
		0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,
		0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87
};
u8 RM295_Gamma_b[62]=
{
    0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,
		0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA4,0xA5,0xA6,0xA7,0xAC,0xAD,0xAE,0xAF,
		0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,
		0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xc6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD
};
//----------------------------------------------------------------------------------
u8 gamma_read_num=0;
u8 gamma_write_num=0;
/*RM67160 Gamma寄存器位址*/
u8 RM160_Gamma_r[22]={0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x0A,0x0B,0x10,0x11,0x14,0x15,0x18,0x19,0x1C,0x1D,0x20,0x21,0x32,0x33};
u8 RM160_Gamma_g[22]={0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x40,0x41,0x46,0x47,0x4A,0x4B,0x4E,0x4F,0x52,0x53,0x56,0x58,0x67,0x68};
u8 RM160_Gamma_b[22]={0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x73,0x74,0x79,0x7A,0x7D,0x7E,0x81,0x82,0x85,0x86,0x89,0x8A,0x99,0x9A};

/*RM67162 Gamma寄存器位址*/
u8 RM162_Gamma_r[50]={0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0c,0x0D,0x0e,0x0f,0x10,0x11,0x12,0x13,0x14,0x15,
                        0x16,0x17,0x18,0x19,0x1a,0x1b,0x1C,0x1D,0x1e,0x1f,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2d,0x2f,0x30,0x31,0x32,0x33};
u8 RM162_Gamma_g[50]={0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3d,0x3f,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,
                        0x4c,0x4d,0x4E,0x4F,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68};
u8 RM162_Gamma_b[50]={0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7b,0x7c,0x7D,0x7E,
                        0x7f,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A};

/** @defgroup usbd_cdc_Private_Macros
  * @{
  */ 
/**
  * @}
  */ 
__IO  uint8_t USB_StatusDataSended = 1;
__IO  uint32_t USB_ReceivedCount = 0;
/** @defgroup usbd_cdc_Private_FunctionPrototypes
  * @{
  */

/*********************************************
   CDC Device library callbacks
 *********************************************/
static uint8_t  usbd_cdc_Init        (void  *pdev, uint8_t cfgidx);
static uint8_t  usbd_cdc_DeInit      (void  *pdev, uint8_t cfgidx);
static uint8_t  usbd_cdc_Setup       (void  *pdev, USB_SETUP_REQ *req);
static uint8_t  usbd_cdc_EP0_RxReady  (void *pdev);
static uint8_t  usbd_cdc_DataIn      (void *pdev, uint8_t epnum);
static uint8_t  usbd_cdc_DataOut     (void *pdev, uint8_t epnum);
static uint8_t  usbd_cdc_SOF         (void *pdev);

/*********************************************
   CDC specific management functions
 *********************************************/
static void Handle_USBAsynchXfer  (void *pdev);
static uint8_t  *USBD_cdc_GetCfgDesc (uint8_t speed, uint16_t *length);
#ifdef USE_USB_OTG_HS  
static uint8_t  *USBD_cdc_GetOtherCfgDesc (uint8_t speed, uint16_t *length);
#endif
/**
  * @}
  */ 

/** @defgroup usbd_cdc_Private_Variables
  * @{
  */ 
extern CDC_IF_Prop_TypeDef  APP_FOPS;
extern uint8_t USBD_DeviceDesc   [USB_SIZ_DEVICE_DESC];

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN uint8_t usbd_cdc_CfgDesc  [USB_CDC_CONFIG_DESC_SIZ] __ALIGN_END ;

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN uint8_t usbd_cdc_OtherCfgDesc  [USB_CDC_CONFIG_DESC_SIZ] __ALIGN_END ;

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN static __IO uint32_t  usbd_cdc_AltSet  __ALIGN_END = 0;

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN uint8_t USB_Rx_Buffer   [CDC_DATA_MAX_PACKET_SIZE] __ALIGN_END ;

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN uint8_t APP_Rx_Buffer   [APP_RX_DATA_SIZE] __ALIGN_END ;  //其实是一个循环缓冲区


#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN uint8_t CmdBuff[CDC_CMD_PACKET_SZE] __ALIGN_END ;

uint32_t APP_Rx_ptr_in  = 0;    //APP_Rx_Buffer指明了其数据进来的位置,当USART 接收到数据时，将数据存储于 APP_Rx_ptr_in 指定的位置
uint32_t APP_Rx_ptr_out = 0;    //APP_Rx_Buffer指明其数据取出 的位置，当 USB 到 FIFO 中取出数据时，起始地址由 APP_Rx_ptr_out 决定
uint32_t APP_Rx_length  = 0;

uint8_t  USB_Tx_State = 0;

static uint32_t cdcCmd = 0xFF;
static uint32_t cdcLen = 0;

/* CDC interface class callbacks structure */
USBD_Class_cb_TypeDef  USBD_CDC_cb = 
{
  usbd_cdc_Init,
  usbd_cdc_DeInit,
  usbd_cdc_Setup,
  NULL,                 /* EP0_TxSent, */
  usbd_cdc_EP0_RxReady,
  usbd_cdc_DataIn,
  usbd_cdc_DataOut,
  usbd_cdc_SOF,
  NULL,
  NULL,     
  USBD_cdc_GetCfgDesc,
#ifdef USE_USB_OTG_HS   
  USBD_cdc_GetOtherCfgDesc, /* use same cobfig as per FS */
#endif /* USE_USB_OTG_HS  */
};

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
/* USB CDC device Configuration Descriptor */
__ALIGN_BEGIN uint8_t usbd_cdc_CfgDesc[USB_CDC_CONFIG_DESC_SIZ]  __ALIGN_END =
{
  /*Configuration Descriptor*/
  0x09,   /* bLength: Configuration Descriptor size */
  USB_CONFIGURATION_DESCRIPTOR_TYPE,      /* bDescriptorType: Configuration */
  USB_CDC_CONFIG_DESC_SIZ,                /* wTotalLength:no of returned bytes */
  0x00,
  0x02,   /* bNumInterfaces: 2 interface */
  0x01,   /* bConfigurationValue: Configuration value */
  0x00,   /* iConfiguration: Index of string descriptor describing the configuration */
  0xC0,   /* bmAttributes: self powered */
  0x32,   /* MaxPower 0 mA */
  
  /*---------------------------------------------------------------------------*/
  
  /*Interface Descriptor */
  0x09,   /* bLength: Interface Descriptor size */
  USB_INTERFACE_DESCRIPTOR_TYPE,  /* bDescriptorType: Interface */
  /* Interface descriptor type */
  0x00,   /* bInterfaceNumber: Number of Interface */
  0x00,   /* bAlternateSetting: Alternate setting */
  0x01,   /* bNumEndpoints: One endpoints used */
  0x02,   /* bInterfaceClass: Communication Interface Class */
  0x02,   /* bInterfaceSubClass: Abstract Control Model */
  0x01,   /* bInterfaceProtocol: Common AT commands */
  0x00,   /* iInterface: */
  
  /*Header Functional Descriptor*/
  0x05,   /* bLength: Endpoint Descriptor size */
  0x24,   /* bDescriptorType: CS_INTERFACE */
  0x00,   /* bDescriptorSubtype: Header Func Desc */
  0x10,   /* bcdCDC: spec release number */
  0x01,
  
  /*Call Management Functional Descriptor*/
  0x05,   /* bFunctionLength */
  0x24,   /* bDescriptorType: CS_INTERFACE */
  0x01,   /* bDescriptorSubtype: Call Management Func Desc */
  0x00,   /* bmCapabilities: D0+D1 */
  0x01,   /* bDataInterface: 1 */
  
  /*ACM Functional Descriptor*/
  0x04,   /* bFunctionLength */
  0x24,   /* bDescriptorType: CS_INTERFACE */
  0x02,   /* bDescriptorSubtype: Abstract Control Management desc */
  0x02,   /* bmCapabilities */
  
  /*Union Functional Descriptor*/
  0x05,   /* bFunctionLength */
  0x24,   /* bDescriptorType: CS_INTERFACE */
  0x06,   /* bDescriptorSubtype: Union func desc */
  0x00,   /* bMasterInterface: Communication class interface */
  0x01,   /* bSlaveInterface0: Data Class Interface */
  
  /*Endpoint 2 Descriptor*/
  0x07,                           /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE,   /* bDescriptorType: Endpoint */
  CDC_CMD_EP,                     /* bEndpointAddress */
  0x03,                           /* bmAttributes: Interrupt */
  LOBYTE(CDC_CMD_PACKET_SZE),     /* wMaxPacketSize: */
  HIBYTE(CDC_CMD_PACKET_SZE),
#ifdef USE_USB_OTG_HS
  0x10,                           /* bInterval: */
#else
  0xFF,                           /* bInterval: */
#endif /* USE_USB_OTG_HS */
  
  /*---------------------------------------------------------------------------*/
  
  /*Data class interface descriptor*/
  0x09,   /* bLength: Endpoint Descriptor size */
  USB_INTERFACE_DESCRIPTOR_TYPE,  /* bDescriptorType: */
  0x01,   /* bInterfaceNumber: Number of Interface */
  0x00,   /* bAlternateSetting: Alternate setting */
  0x02,   /* bNumEndpoints: Two endpoints used */
  0x0A,   /* bInterfaceClass: CDC */
  0x00,   /* bInterfaceSubClass: */
  0x00,   /* bInterfaceProtocol: */
  0x00,   /* iInterface: */
  
  /*Endpoint OUT Descriptor*/
  0x07,   /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE,      /* bDescriptorType: Endpoint */
  CDC_OUT_EP,                        /* bEndpointAddress */
  0x02,                              /* bmAttributes: Bulk */
  LOBYTE(CDC_DATA_MAX_PACKET_SIZE),  /* wMaxPacketSize: */
  HIBYTE(CDC_DATA_MAX_PACKET_SIZE),
  0x00,                              /* bInterval: ignore for Bulk transfer */
  
  /*Endpoint IN Descriptor*/
  0x07,   /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE,      /* bDescriptorType: Endpoint */
  CDC_IN_EP,                         /* bEndpointAddress */
  0x02,                              /* bmAttributes: Bulk */
  LOBYTE(CDC_DATA_MAX_PACKET_SIZE),  /* wMaxPacketSize: */
  HIBYTE(CDC_DATA_MAX_PACKET_SIZE),
  0x00                               /* bInterval: ignore for Bulk transfer */
} ;

#ifdef USE_USB_OTG_HS
#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */ 
__ALIGN_BEGIN uint8_t usbd_cdc_OtherCfgDesc[USB_CDC_CONFIG_DESC_SIZ]  __ALIGN_END =
{ 
  0x09,   /* bLength: Configuation Descriptor size */
  USB_DESC_TYPE_OTHER_SPEED_CONFIGURATION,   
  USB_CDC_CONFIG_DESC_SIZ,
  0x00,
  0x02,   /* bNumInterfaces: 2 interfaces */
  0x01,   /* bConfigurationValue: */
  0x04,   /* iConfiguration: */
  0xC0,   /* bmAttributes: */
  0x32,   /* MaxPower 100 mA */  
  
  /*Interface Descriptor */
  0x09,   /* bLength: Interface Descriptor size */
  USB_INTERFACE_DESCRIPTOR_TYPE,  /* bDescriptorType: Interface */
  /* Interface descriptor type */
  0x00,   /* bInterfaceNumber: Number of Interface */
  0x00,   /* bAlternateSetting: Alternate setting */
  0x01,   /* bNumEndpoints: One endpoints used */
  0x02,   /* bInterfaceClass: Communication Interface Class */
  0x02,   /* bInterfaceSubClass: Abstract Control Model */
  0x01,   /* bInterfaceProtocol: Common AT commands */
  0x00,   /* iInterface: */
  
  /*Header Functional Descriptor*/
  0x05,   /* bLength: Endpoint Descriptor size */
  0x24,   /* bDescriptorType: CS_INTERFACE */
  0x00,   /* bDescriptorSubtype: Header Func Desc */
  0x10,   /* bcdCDC: spec release number */
  0x01,
  
  /*Call Management Functional Descriptor*/
  0x05,   /* bFunctionLength */
  0x24,   /* bDescriptorType: CS_INTERFACE */
  0x01,   /* bDescriptorSubtype: Call Management Func Desc */
  0x00,   /* bmCapabilities: D0+D1 */
  0x01,   /* bDataInterface: 1 */
  
  /*ACM Functional Descriptor*/
  0x04,   /* bFunctionLength */
  0x24,   /* bDescriptorType: CS_INTERFACE */
  0x02,   /* bDescriptorSubtype: Abstract Control Management desc */
  0x02,   /* bmCapabilities */
  
  /*Union Functional Descriptor*/
  0x05,   /* bFunctionLength */
  0x24,   /* bDescriptorType: CS_INTERFACE */
  0x06,   /* bDescriptorSubtype: Union func desc */
  0x00,   /* bMasterInterface: Communication class interface */
  0x01,   /* bSlaveInterface0: Data Class Interface */
  
  /*Endpoint 2 Descriptor*/
  0x07,                           /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE,   /* bDescriptorType: Endpoint */
  CDC_CMD_EP,                     /* bEndpointAddress */
  0x03,                           /* bmAttributes: Interrupt */
  LOBYTE(CDC_CMD_PACKET_SZE),     /* wMaxPacketSize: */
  HIBYTE(CDC_CMD_PACKET_SZE),
  0xFF,                           /* bInterval: */
  
  /*---------------------------------------------------------------------------*/
  
  /*Data class interface descriptor*/
  0x09,   /* bLength: Endpoint Descriptor size */
  USB_INTERFACE_DESCRIPTOR_TYPE,  /* bDescriptorType: */
  0x01,   /* bInterfaceNumber: Number of Interface */
  0x00,   /* bAlternateSetting: Alternate setting */
  0x02,   /* bNumEndpoints: Two endpoints used */
  0x0A,   /* bInterfaceClass: CDC */
  0x00,   /* bInterfaceSubClass: */
  0x00,   /* bInterfaceProtocol: */
  0x00,   /* iInterface: */
  
  /*Endpoint OUT Descriptor*/
  0x07,   /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE,      /* bDescriptorType: Endpoint */
  CDC_OUT_EP,                        /* bEndpointAddress */
  0x02,                              /* bmAttributes: Bulk */
  0x40,                              /* wMaxPacketSize: */
  0x00,
  0x00,                              /* bInterval: ignore for Bulk transfer */
  
  /*Endpoint IN Descriptor*/
  0x07,   /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE,     /* bDescriptorType: Endpoint */
  CDC_IN_EP,                        /* bEndpointAddress */
  0x02,                             /* bmAttributes: Bulk */
  0x40,                             /* wMaxPacketSize: */
  0x00,
  0x00                              /* bInterval */
};
#endif /* USE_USB_OTG_HS  */




/**
  * @}
  */ 

/** @defgroup usbd_cdc_Private_Functions
  * @{
  */ 

/**
  * @brief  usbd_cdc_Init
  *         Initilaize the CDC interface
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  usbd_cdc_Init (void  *pdev, 
                               uint8_t cfgidx)
{
  uint8_t *pbuf;

  /* Open EP IN */
  DCD_EP_Open(pdev,
              CDC_IN_EP,
              CDC_DATA_IN_PACKET_SIZE,
              USB_OTG_EP_BULK);
  
  /* Open EP OUT */
  DCD_EP_Open(pdev,
              CDC_OUT_EP,
              CDC_DATA_OUT_PACKET_SIZE,
              USB_OTG_EP_BULK);
  
  /* Open Command IN EP */
  DCD_EP_Open(pdev,
              CDC_CMD_EP,
              CDC_CMD_PACKET_SZE,
              USB_OTG_EP_INT);
  
  pbuf = (uint8_t *)USBD_DeviceDesc;
  pbuf[4] = DEVICE_CLASS_CDC;
  pbuf[5] = DEVICE_SUBCLASS_CDC;
  
  /* Initialize the Interface physical components */
  APP_FOPS.pIf_Init();

  /* Prepare Out endpoint to receive next packet */
  DCD_EP_PrepareRx(pdev,
                   CDC_OUT_EP,
                   (uint8_t*)(USB_Rx_Buffer),
                   CDC_DATA_OUT_PACKET_SIZE);
  
  return USBD_OK;
}

/**
  * @brief  usbd_cdc_Init
  *         DeInitialize the CDC layer
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  usbd_cdc_DeInit (void  *pdev, 
                                 uint8_t cfgidx)
{
  /* Open EP IN */
  DCD_EP_Close(pdev,
              CDC_IN_EP);
  
  /* Open EP OUT */
  DCD_EP_Close(pdev,
              CDC_OUT_EP);
  
  /* Open Command IN EP */
  DCD_EP_Close(pdev,
              CDC_CMD_EP);

  /* Restore default state of the Interface physical components */
  APP_FOPS.pIf_DeInit();
  
  return USBD_OK;
}

/**
  * @brief  usbd_cdc_Setup
  *         Handle the CDC specific requests
  * @param  pdev: instance
  * @param  req: usb requests
  * @retval status
  */
static uint8_t  usbd_cdc_Setup (void  *pdev, 
                                USB_SETUP_REQ *req)
{
  uint16_t len;
  uint8_t  *pbuf;
  
  switch (req->bmRequest & USB_REQ_TYPE_MASK)
  {
    /* CDC Class Requests -------------------------------*/
  case USB_REQ_TYPE_CLASS :
      /* Check if the request is a data setup packet */
      if (req->wLength)
      {
        /* Check if the request is Device-to-Host */
        if (req->bmRequest & 0x80)
        {
          /* Get the data to be sent to Host from interface layer */
          APP_FOPS.pIf_Ctrl(req->bRequest, CmdBuff, req->wLength);
          
          /* Send the data to the host */
          USBD_CtlSendData (pdev, 
                            CmdBuff,
                            req->wLength);          
        }
        else /* Host-to-Device requeset */
        {
          /* Set the value of the current command to be processed */
          cdcCmd = req->bRequest;
          cdcLen = req->wLength;
          
          /* Prepare the reception of the buffer over EP0
          Next step: the received data will be managed in usbd_cdc_EP0_TxSent() 
          function. */
          USBD_CtlPrepareRx (pdev,
                             CmdBuff,
                             req->wLength);          
        }
      }
      else /* No Data request */
      {
        /* Transfer the command to the interface layer */
        APP_FOPS.pIf_Ctrl(req->bRequest, NULL, 0);
      }
      
      return USBD_OK;
      
    default:
      USBD_CtlError (pdev, req);
      return USBD_FAIL;
    
      
      
    /* Standard Requests -------------------------------*/
  case USB_REQ_TYPE_STANDARD:
    switch (req->bRequest)
    {
    case USB_REQ_GET_DESCRIPTOR: 
      if( (req->wValue >> 8) == CDC_DESCRIPTOR_TYPE)
      {
#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
        pbuf = usbd_cdc_Desc;   
#else
        pbuf = usbd_cdc_CfgDesc + 9 + (9 * USBD_ITF_MAX_NUM);
#endif 
        len = MIN(USB_CDC_DESC_SIZ , req->wLength);
      }
      
      USBD_CtlSendData (pdev, 
                        pbuf,
                        len);
      break;
      
    case USB_REQ_GET_INTERFACE :
      USBD_CtlSendData (pdev,
                        (uint8_t *)&usbd_cdc_AltSet,
                        1);
      break;
      
    case USB_REQ_SET_INTERFACE :
      if ((uint8_t)(req->wValue) < USBD_ITF_MAX_NUM)
      {
        usbd_cdc_AltSet = (uint8_t)(req->wValue);
      }
      else
      {
        /* Call the error management function (command will be nacked */
        USBD_CtlError (pdev, req);
      }
      break;
    }
  }
  return USBD_OK;
}

/**
  * @brief  usbd_cdc_EP0_RxReady
  *         Data received on control endpoint
  * @param  pdev: device device instance
  * @retval status
  */
static uint8_t  usbd_cdc_EP0_RxReady (void  *pdev)
{ 
  if (cdcCmd != NO_CMD)
  {
    /* Process the data */
    APP_FOPS.pIf_Ctrl(cdcCmd, CmdBuff, cdcLen);
    
    /* Reset the command variable to default value */
    cdcCmd = NO_CMD;
  }
  
  return USBD_OK;
}

/**
  * @brief  usbd_audio_DataIn
  *         Data sent on non-control IN endpoint
  * @param  pdev: device instance
  * @param  epnum: endpoint number
  * @retval status
  */
static uint8_t  usbd_cdc_DataIn (void *pdev, uint8_t epnum)
{
  if(epnum == (CDC_IN_EP&0x7F))
  {
    //DCD_EP_Flush(pdev, CDC_IN_EP);
    USB_StatusDataSended = 1;
  }
  return USBD_OK;
}

void SSD2828_signal_on(u8 signal_mode,u8 channel)
{
	for(i = 0;i<SSD28xxCode_count-1;i ++)	//将SSD2828的配置参数 通过SPI写入到 芯片
	{
		SSD2828_W_Reg(signal_mode,channel,(u8)(SSD28xx_Code[i]>>16),(u16)SSD28xx_Code[i]);
	}
	delay_ms(20);
	for(i=0;i<InitCode_count;i++)			//将初始化代码 写入2828,通过2828的低速模式传送
	{		
		SSD2828_W_Array(signal_mode,channel,InitCode_valid[i],0);
		
	}

    FSMC_Pure_colour(BLACK); 
    Load_done_HIGH;

    SSD2828_W_Reg(signal_mode,channel,(u8)(SSD28xx_Code[SSD28xxCode_count-1]>>16),(u16)SSD28xx_Code[SSD28xxCode_count-1]);
 #if InsertBlack == 1   
    delay_ms(100);
#endif
}

void SPI_signal_on(u8 SPI_Mode,u8 channel)
{
	for(i = 0;i<InitCode_count-2;i++)			
	{			
		SPI_Write_u8_Array(SPI_Mode,channel,(u8 *)InitCode_valid[i]);
	}
    
	SPI_Write_u8_Cmd(SPI_Mode,channel,0x11);
	delay_ms(20);
	SPI_Write_u8_Cmd(SPI_Mode,channel,0x29);
	delay_ms(20);    
}
/**
  * @brief  usbd_audio_DataOut
  *         Data received on non-control Out endpoint
  * @param  pdev: device instance
  * @param  epnum: endpoint number
  * @retval status
  */
static uint8_t  usbd_cdc_DataOut (void *pdev, uint8_t epnum)
{      
    u16 i=0;
    u16 tmp;
	if(epnum == (CDC_OUT_EP&0x7F))
	{
        USB_ReceivedCount = USBD_GetRxCount(pdev,epnum);
        /* Prapare EP to Receive next Cmd */
        DCD_EP_PrepareRx (pdev,CDC_OUT_EP,(uint8_t *)USB_Rx_Buffer,sizeof(USB_Rx_Buffer)); 
        if(USB_ReceivedCount>1)
        {
            if((USB_ReceivedCount==REC_LCDCONF_NUM)&&(NUM==0))      //接受的数据长度 固定   
            {
                if(USB_Rx_Buffer[0]==REC_LCDCONF_START)         //上位机 发送模组Timing参数、ICS307配置参数、通道数
                {
                    Uart_rx_flag = USB_Rx_Buffer[1];
                    crc16_data = crc16(USB_Rx_Buffer,USB_Rx_Buffer[2]+4-2,0xFFFF);
                    if(crc16_data == ((USB_Rx_Buffer[USB_Rx_Buffer[2]+4-2]<<8)|(USB_Rx_Buffer[USB_Rx_Buffer[2]+4-1])))
                    {
                        if(REC_W_LCDCONF_FLAG == Uart_rx_flag)		//上位机 发送 LCDConfig 数据
                        {       
                            FPGA_ResetProcess();
                            Load_done_LOW;
                            ISC307_config(ICS307_Buf_Data);		//配置ICS307芯片输出pixel_clk
                            delay_ms(15);
                            
                            
                            //配置ICS307时钟芯片
                            ICS307_Valid_Data = (u32)(USB_Rx_Buffer[16]<<24)|(u32)(USB_Rx_Buffer[15]<<16)|(u32)(USB_Rx_Buffer[14]<<8)|(u32)(USB_Rx_Buffer[13]);
                            OLED.DisMode = ((USB_Rx_Buffer[18]&0x0f)<2) ? 0 : 1;
                            OLED.H_pixel = (OLED.DisMode == 0) ? (u16)(USB_Rx_Buffer[3]<<8)|(USB_Rx_Buffer[4]) : 2*((u16)(USB_Rx_Buffer[3]<<8)|(USB_Rx_Buffer[4]));
                            OLED.V_pixel =  (u16)(USB_Rx_Buffer[8]<<8)|(USB_Rx_Buffer[9]);
                            OLED.pixel_clk = (float)((((u16)(USB_Rx_Buffer[21]<<8)+USB_Rx_Buffer[20]))/10.0);
                            if(OLED.pixel_clk<30)
                                FSMC_IO_init(0x30);			//FSMC接口的引脚初始化
                            else
                                FSMC_IO_init(0x06);			//FSMC接口的引脚初始化                            
                            
                            OLED.SigMode = USB_Rx_Buffer[18]&0XF0;
                            if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))
                            {
                                write_cmd(0x1234);						//将OLED时序数据发送到FPGA
                                delay_us(2);
                                write_cmd((u16)(USB_Rx_Buffer[4]<<8)|(USB_Rx_Buffer[3]) );	
                                delay_us(2);
                                write_cmd((u16)(USB_Rx_Buffer[9]<<8)|(USB_Rx_Buffer[8]) );
                                delay_us(2);                                
                                write_cmd((u16)(USB_Rx_Buffer[6]<<8) ) ;
                                delay_us(2);                                
                                write_cmd((u16)(USB_Rx_Buffer[11]<<8) ) ;	
                                delay_us(2);
                                write_cmd((u16)(USB_Rx_Buffer[5]<<8) ) ;	
                                delay_us(2);
                                write_cmd((u16)(USB_Rx_Buffer[10]<<8 ) );	
                                delay_us(2);
                                write_cmd((u16)(USB_Rx_Buffer[7]<<8) ) ;	
                                delay_us(2);
                                write_cmd((u16)(USB_Rx_Buffer[12]<<8) ) ;	       
                                delay_us(2);                                
                                write_cmd((u16)(USB_Rx_Buffer[18]<<8) + ((OLED.SigMode == Mipi_Mode) ? 0x00 : 0x01 )) ;	
                                delay_us(2);
                                write_cmd((u16)(USB_Rx_Buffer[18]<<8) + ((OLED.SigMode == Mipi_Mode) ? 0x00 : 0x01 )) ;	
                                delay_us(2);
                                write_cmd(0x4321); 
                            }                        

                            CS_Master = CS1;
                            CS_Slaver = CS2;                               
                            SSD28xxCode_count = 0;
                            InitCode_count = 0;
                            Set_2828_reset(OLED.SigMode);       //复位2828和OLED模组
                            STM2PC_ERROR(pdev,CDC_IN_EP,USB_Rx_Buffer[0],Uart_rx_flag,Uart_Error_None,0);           //返回 写LCD_Config状态  ：ok                       
                        }
                        else if(REC_R_LCDCONF_FLAG == Uart_rx_flag)	//上位机 读取 LCDConfig 数据
                        {										
                            i = 0;
                            WriteAddress = Inter_Flash_Addr;
                            FLASH_If_Read(&WriteAddress, buffer1, 0,2048);	//将FLASH的模组Timing数据读取出来，准备发送给FPGA
                            for(i=0;i<16;i++)
                            {
                                    USB_Rx_Buffer[i*4]     = (u8)(buffer1[i]>>24);
                                    USB_Rx_Buffer[(i*4)+1] = (u8)(buffer1[i]>>16);
                                    USB_Rx_Buffer[(i*4)+2] = (u8)(buffer1[i]>>8);
                                    USB_Rx_Buffer[(i*4)+3] = (u8)(buffer1[i]);
                            }									
                            if((USB_Rx_Buffer[0] == 0x47)&&(USB_Rx_Buffer[1] == 0x56)&&(USB_Rx_Buffer[2] == 0x4F)&&(USB_Rx_Buffer[3] == 0x43)&&(USB_Rx_Buffer[4] == 0x6F)&&(USB_Rx_Buffer[5] == 0x6E))
                                STM2PC_LCDConfig(pdev,CDC_IN_EP,Uart_rx_flag,USB_Rx_Buffer,Uart_Error_None);       //返回 读LCD_Config状态 ：ok；将LCD_Config数据返回
                            else
                                STM2PC_LCDConfig(pdev,CDC_IN_EP,Uart_rx_flag,USB_Rx_Buffer,Uart_Error_IntFlash);	//返回 flash内部没有配置文件 ：error									
                    
                        }
                        else if(REC_W_PATTERN_FLAG == Uart_rx_flag)	//上位机 发送 画面颜色 数据
                        {	
							STM2PC_ERROR(pdev,CDC_IN_EP,USB_Rx_Buffer[0],Uart_rx_flag,Uart_Error_None,0);
                            if(OLED.SigMode == Mipi_Mode)
                            {       
#if DSI_Set_Window_EN == 1
                                SSD2828_Set_Window(OLED.SigMode,CS_Master,(OLED.H_pixel==390) ? 4 : 0,(OLED.H_pixel==390) ? OLED.H_pixel+4 : OLED.H_pixel,0,OLED.V_pixel);
#endif                                
                                ICS307_HighLow_Set(OLED.pixel_clk,ICS307_Buf_Data); 

                                FSMC_Pure_colour((u32)(USB_Rx_Buffer[3]<<16)|((u32)(USB_Rx_Buffer[4]<<8)|USB_Rx_Buffer[5]));
                                ICS307_ValidClk_Set(ICS307_Valid_Data); 
                                Load_done_HIGH;
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                PatternR = USB_Rx_Buffer[3];
                                PatternG = USB_Rx_Buffer[4];
                                PatternB = USB_Rx_Buffer[5]; 
                            }
							else if(OLED.SigMode == CMD_Mode)           //Command 模式
		                    {
#if DSI_Set_Window_EN == 1                    
                                SSD2828_Set_Window(OLED.SigMode,CS_Master,(OLED.H_pixel==390) ? 4 : 0,(OLED.H_pixel==390) ? OLED.H_pixel+4 : OLED.H_pixel,0,OLED.V_pixel);
#endif
		                        SSD2828_W_RAM_Pic(OLED.pixel_clk,OLED.SigMode,CS_Master,OLED.H_pixel,OLED.V_pixel,USB_Rx_Buffer[3],USB_Rx_Buffer[4],USB_Rx_Buffer[5]);                        
		                    }
		                    else if((OLED.SigMode == SPI3_Mode)||(OLED.SigMode == SPI4_Mode))          //3线8bit SPI 模式   //4线8bit SPI 模式
		                    {
#if SPI_Set_Window_EN == 1                                 
		                        SPI_Set_Window(OLED.SigMode,CS_Master,0, OLED.H_pixel-1,0, OLED.V_pixel-1);
#endif
		                        SPI3_WriteRAM(OLED.SigMode,CS_Master,OLED.H_pixel,OLED.V_pixel,USB_Rx_Buffer[3],USB_Rx_Buffer[4],USB_Rx_Buffer[5]);
		                    }

                        }
                        else if(REC_R_PATTERN_FLAG == Uart_rx_flag)	//上位机 读取 画面颜色 数据
                        {		
                            STM2PC_Pattern(pdev,CDC_IN_EP,USB_Rx_Buffer[0],PatternR,PatternG,PatternB,Uart_Error_None);
                        }
                        else if(REC_R_SW_VERSION_FLAG == Uart_rx_flag)	//上位机 读取 SW 版本号
                        {
                            STM2PC_SW_Version(pdev,CDC_IN_EP,USB_Rx_Buffer[0],Uart_rx_flag,Uart_Error_None,FW_Version, FW_SubVersion); 
                        }
                    }
                }                                                                      
            }
            else if(USB_Rx_Buffer[0]==REC_UPDATABIN_Start)      //接受的 数据 长度非固定
            {
                NVIC_SystemReset();
            }
            else if(((USB_Rx_Buffer[0]==REC_SSD_CODE_START)&&(NUM==0))||(Receive_Code_flag == 1))      //接受的 数据 长度非固定
            {
                if(Receive_Code_flag == 0)
                {
                    Receive_Code_flag = 1;
                    Uart_rx_flag=USB_Rx_Buffer[1]&0x0f;
                }
                for(i=0;i<USB_ReceivedCount;i++)
                    buffer[i+VCP_Receive_True_num] = USB_Rx_Buffer[i];
                
                VCP_Receive_True_num+=USB_ReceivedCount;
                
                
                if(VCP_Receive_True_num==(buffer[1]>>4)*256+buffer[2]+5)
                {
                    Receive_Code_flag = 0;
                    if((REC_W_SSD_ONE_FLAG == Uart_rx_flag)||(REC_W_SSD_FLAG == Uart_rx_flag))		//上位机  发送 SSD2828 命令
                    {
                        SSD28xxCode_count = 0;
                        STM2PC_ERROR(pdev,CDC_IN_EP,buffer[0],Uart_rx_flag,Uart_Error_None,0);
                        if(REC_W_SSD_ONE_FLAG == Uart_rx_flag)
                        {
                            if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI
                            {      
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,buffer[3],(u16)(buffer[4]<<8)|buffer[5]);
                            }
                            else if(OLED.SigMode == CMD_Mode)//Command
                            {
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,buffer[3],(u16)(buffer[4]<<8)|buffer[5]);                                 
                            }
                            if(buffer[3] == 0xB7)
                            {
                                if(((u16)(buffer[4]<<8)|buffer[5])&0x0001)
                                    HS_B7_Data = (u16)(buffer[4]<<8)|buffer[5];
                                else
                                    LP_B7_Data = (u16)(buffer[4]<<8)|buffer[5];
                            }                           
                        }
                        else
                        {
                            if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))
                            {
                                for (i = 0; i < 512; i++)		//将2828的配置数据 转换成字
                                {
                                    if (buffer[i * 3 +3] == 0xff)
                                    {
                                            break;                    
                                    }
                                    //    0x H H H H  :   31:24->00(没作用)   23:16->寄存器地址    15:0->寄存器值(高位在前)
                                    SSD28xx_Code[SSD28xxCode_count] = ((uint32_t)buffer[i * 3 +3]<<16) + ((uint32_t)buffer[i * 3 + 1+3]<<8) + ((uint32_t)buffer[i * 3 + 2+3]);
                                    SSD28xxCode_count ++;	
                                    if(buffer[i * 3 +3] == 0xB7)
                                    {
                                        if(((u16)(buffer[i * 3 +4]<<8)|buffer[i * 3 +5])&0x0001)
                                        {
                                            HS_B7_Data = (u16)(buffer[i * 3 +4]<<8)|buffer[i * 3 +5];
                                        }
                                        else
                                            LP_B7_Data = (u16)(buffer[i * 3 +4]<<8)|buffer[i * 3 +5];                                         
                                    }                                        
                                }		
                            }                                
                            
                        } 

                    }
                    else if((REC_W_CODE_ONE_FLAG == Uart_rx_flag)||(REC_W_CODE_FLAG == Uart_rx_flag))		//上位机  发送 DriverIC code
                    {	
                        InitCode_count = 0;
                        STM2PC_ERROR(pdev,CDC_IN_EP,buffer[0],Uart_rx_flag,Uart_Error_None,0);
                        if(REC_W_CODE_ONE_FLAG == Uart_rx_flag)
                        {
                            if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))  //RGB+SPI= MIPI
                            {									
                                SSD2828_W_Array(OLED.SigMode,CS_Master,buffer,2);
                            }
                            else if((OLED.SigMode == SPI3_Mode)||(OLED.SigMode == SPI4_Mode))          //3线8bit SPI 模式   //4线8bit SPI 模式
                            {
                                for(i=0;i<buffer[2]+1;i++)
                                    buffer[i] = buffer[i+2];
                                if(buffer[2] > 1)
                                {
                                    SPI_Write_u8_Array(OLED.SigMode,CS_Master,buffer);
                                }
                                else if(buffer[2] == 1)
                                {
                                    SPI_Write_u8_Cmd(OLED.SigMode,CS_Master,buffer[1]);
                                    delay_ms(30);

                                }  
                            }
                        }
                        else
                        {
                                for (i = 3; i < 4096; i += (Code_length+1))
                                {
                                    Code_length = buffer[i];	//每一句初始化代码的 数据长度

                                    if (Code_length == 0xff)	//判断长度有255说明 ，到初始化代码的最后
                                    {
                                            break;
                                    }
                                    Timing_and_InitCode[0] =	Code_length;	//数组中的[0]：这一句初始化代码的数据长度									
                                    for (j = 1; j < Code_length+1; j++)	//递增将 初始化代码写入到数组
                                    {											
                                            Timing_and_InitCode[j] = ((uint32_t)buffer[i+j]);
                                    }									
                                    for(j=0;j<Timing_and_InitCode[0]+1;j++)												
                                        InitCode_valid[InitCode_count][j] = (u8)Timing_and_InitCode[j];
                                    
                                    InitCode_count ++;	//即将写下一个数组(下一句初始化代码)																											
                                }
                                if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))  //RGB+SPI= MIPI
                                {									
                                    SSD2828_signal_on(OLED.SigMode,CS_Master);
                                }
                                else if((OLED.SigMode == SPI3_Mode)||(OLED.SigMode == SPI4_Mode))          //3线8bit SPI 模式   //4线8bit SPI 模式
                                {
                                    SPI_signal_on(OLED.SigMode,CS_Master);
                                }

                        }
                        
                    }
                    else if(REC_R_CODE_FLAG == Uart_rx_flag)		//上位机  读取 DriverIC code
                    {
                    
                    }    
                    VCP_Receive_True_num = 0;
                }                
            }
            else if((USB_ReceivedCount==USB_Rx_Buffer[2]+5)&&(NUM==0))      //接受的 数据 长度非固定
            {
                Uart_rx_flag=USB_Rx_Buffer[1]&0x0f;
                crc16_data = crc16(USB_Rx_Buffer,USB_Rx_Buffer[2]+3,0xFFFF);
                if(crc16_data == ((USB_Rx_Buffer[USB_Rx_Buffer[2]+3]<<8)|(USB_Rx_Buffer[USB_Rx_Buffer[2]+4])))
                {                
                    if(USB_Rx_Buffer[0]==Rec_R_Config_Start)
                    {
                        if(Rec_R_Config_flag == Uart_rx_flag)		//上位机  读取  配置文件 命令
                        {
                            WriteAddress = Inter_Flash_Addr;
                            SendTo_PC_cfgFile=1;
                        }                                                                    
                    }  
                    else if(USB_Rx_Buffer[0]==RM67120)
                    {
                        switch (USB_Rx_Buffer[1])
                        {            
                            case 0x01:                                      //写寄存器
                            {
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
                                SSD2828_W_Array(OLED.SigMode,CS_Master,USB_Rx_Buffer,2);                               

								delay_ms(5);
								SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                USB_Rx_Buffer[4] = Uart_Error_None;
                                USB_Rx_Buffer[5] = 0;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 写寄存器状态  ：ok  
                            }
                            break;	
                            case 0x02:                                      //读寄存器
                            {
                                USB_Rx_Buffer[1]=0x03;
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);                               
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC1,USB_Rx_Buffer[2]-1); //return package size                                
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC0,0x0001);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,USB_Rx_Buffer[3]); //
                                delay_ms(5);  
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xFF);
                                for(i=0;i<USB_Rx_Buffer[2]-1;i++)
                                {
                                  SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xFA);		
                                  tmp=SPI3_Read_u16_Data(CS_Master);
                                  USB_Rx_Buffer[4+i]=tmp>>8;
                                  USB_Rx_Buffer[5+i]=tmp;
                                  delay_ms(5);
                                  i++;
                                }

								SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的 寄存器数据  ：ok  	
                            }
                            break;	
                            case 0x08:                                      //写Gamma数据
                            {    
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0006);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);

                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xF0);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x55);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xAA);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x52);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x08);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x01);		// Page 1	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0035); //+1 CMD
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);	
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);	
                                SSD2828_W_Data(OLED.SigMode,CS_Master,((USB_Rx_Buffer[3]==0x00) ? 0xD1 : ((USB_Rx_Buffer[3]==0x42) ? 0xD2 : 0xD3))); //RED//GREEN//BLUE
                                for( i = 0;i<52;i++)   //26阶灰度调整
                                {	                                                                                  
                                    SSD2828_W_Data(OLED.SigMode,CS_Master,USB_Rx_Buffer[i+4]);
                                }
                                delay_ms(5);

								SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                USB_Rx_Buffer[4] = Uart_Error_None;
                                USB_Rx_Buffer[5] = 0;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 写Gamma状态  ：ok  
                            }
                            break;
                            case 0x09:                                      //读Gamma数据
                            {	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data);
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0006);	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xF0);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x55);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xAA);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x52);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x08);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x01);	
                                
                                delay_ms(5);     
                                
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC1,0x0034);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);                        
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,((USB_Rx_Buffer[3]==0x00) ? 0xD1 : ((USB_Rx_Buffer[3]==0x42) ? 0xD2 : 0xD3))); //RED//GREEN//BLUE               
                                delay_ms(5);  
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xFF);
                                for(i=0;i<51;i++)
                                {
                                    SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xFA);		
                                    tmp=SPI3_Read_u16_Data(CS_Master);
                                    USB_Rx_Buffer[4+i]=tmp>>8;
                                    USB_Rx_Buffer[5+i]=tmp;
                                    delay_ms(5);
                                    i++;
                                }                               

                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的 Gamma数据  ：ok  
                                        
                            }
                            break;
                                   
                            case 0x0A://gamma otp
                            {  
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0006);	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xF0);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x55);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xAA);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x52);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x08);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x01);		// Page 1
                                
                                delay_ms(5);  

                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0003);//gamma刻录决定 
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xED);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x40);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x00);
                                
                                delay_ms(5);  
                                
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0004);//刻录标识符	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xEE);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xA5);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x5A);					
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x3C);	                               
                                delay_ms(2000);  //more than 500 ms
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0003);//清除烧录缓存器设定
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xED);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x40);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x00);
                                USB_Rx_Buffer[3] = Uart_Error_None;
                                USB_Rx_Buffer[4] = 0;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的 Gamma数据  ：ok   

                            }
                                    break;
                            case 0x0B:  //回读gamma烧录次数
                            {
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,LP_B7_Data,0x0200);
                                delay_ms(5);            
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0006);	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xF0);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x55);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xAA);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x52);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x08);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x01);		// Page 1
                                delay_ms(5);       
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC1,0x0002);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC0,0x0001);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0xEF); 
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA);
                                delay_ms(90); 
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xFF);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xFA);
                                USB_Rx_Buffer[4]=SPI3_Read_u8_Data(CS_Master);
                                tmp=SPI3_Read_u8_Data(CS_Master);	
                                delay_ms(5);

                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                USB_Rx_Buffer[3] = Uart_Error_None;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的 Gamma数据  ：ok  
                            }
                                    
                            default:	break;	
                            }
                                        
                    }
                    else if((USB_Rx_Buffer[0]==RM67195)||(USB_Rx_Buffer[0]==RM67295)||(USB_Rx_Buffer[0]==RM67198))
                    {
                        switch (USB_Rx_Buffer[1])
                        {            
                            case 0x01:                                      //写寄存器
                            {
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
                                delay_ms(5);
                                SSD2828_W_Array(OLED.SigMode,CS_Master,USB_Rx_Buffer,2);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                delay_ms(5);
															  if(USB_Rx_Buffer[0]==RM67195)
																{
																	SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0xAAFB); 
																}
                                USB_Rx_Buffer[4] = Uart_Error_None;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 写寄存器状态  ：ok  
                            }
                            break;	
                            case 0x02:                                      //读寄存器
                            {
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC1,0x0001);                                
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);                                
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,USB_Rx_Buffer[3]);
                                delay_ms(15);
                                USB_Rx_Buffer[4]=SSD2828_R_Reg(OLED.SigMode,CS_Master,0xFF);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                delay_ms(5);
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的 寄存器数据  ：ok  	
                            }
                            break;	
                            case 0x08:   
                            {											//写Gamma数据
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000); 
                                if((USB_Rx_Buffer[0]==RM67195)||(USB_Rx_Buffer[0]==RM67295))
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x05FE); 
                                else if(USB_Rx_Buffer[0]==RM67198)
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x50FE);
                                for( i = 0;i<62;i++)
                                {	 
                                    if(USB_Rx_Buffer[0]==RM67195)
                                    {
                                        if(USB_Rx_Buffer[3]==0x00)	
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(u16)(USB_Rx_Buffer[i+4]<<8)|RM195_Gamma_r[i]);  
                                        else if(USB_Rx_Buffer[3]==0x42)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(u16)(USB_Rx_Buffer[i+4]<<8)|RM195_Gamma_g[i]); 
                                        else if(USB_Rx_Buffer[3]==0x81)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(u16)(USB_Rx_Buffer[i+4]<<8)|RM195_Gamma_b[i]); 
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0xAAFB); 
                                    }
                                    else if(USB_Rx_Buffer[0]==RM67295)
                                    {
                                        if(USB_Rx_Buffer[3]==0x00)	
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(u16)(USB_Rx_Buffer[i+4]<<8)|RM295_Gamma_r[i]);  
                                        else if(USB_Rx_Buffer[3]==0x42)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(u16)(USB_Rx_Buffer[i+4]<<8)|RM295_Gamma_g[i]); 
                                        else if(USB_Rx_Buffer[3]==0x81)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(u16)(USB_Rx_Buffer[i+4]<<8)|RM295_Gamma_b[i]); 																		
                                    }
                                    else if(USB_Rx_Buffer[0]==RM67198)
                                    {
                                        if(USB_Rx_Buffer[3]==0x00)	
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(u16)(USB_Rx_Buffer[i+4]<<8)|RM295_Gamma_r[i]);  
                                        else if(USB_Rx_Buffer[3]==0x42)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(u16)(USB_Rx_Buffer[i+4]<<8)|RM295_Gamma_g[i]); 
                                        else if(USB_Rx_Buffer[3]==0x81)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(u16)(USB_Rx_Buffer[i+4]<<8)|RM295_Gamma_b[i]); 											
                                    }
                                    delay_ms(2);
                                }	
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                USB_Rx_Buffer[4] = Uart_Error_None;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 写Gamma状态  ：ok  
                            }
                            break;
                            case 0x09:                                      //读Gamma数据
                            {	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data);
                                delay_ms(2);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);	
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                if((USB_Rx_Buffer[0]==RM67195)||(USB_Rx_Buffer[0]==RM67295))
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x05FE); 
                                else if(USB_Rx_Buffer[0]==RM67198)
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x50FE);
                                                               
                                for( i = 0;i<62;i++)							
                                {  	
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080); 
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                    SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                    if(USB_Rx_Buffer[0]==RM67195)
                                    {
                                        SSD2828_W_Data(OLED.SigMode,CS_Master,(USB_Rx_Buffer[3]==0x00) ? RM195_Gamma_r[i] : ((USB_Rx_Buffer[3]==0x42) ? RM195_Gamma_g[i] : RM195_Gamma_b[i]));
                                    }
                                    else if(USB_Rx_Buffer[0]==RM67295)
                                    {
                                        SSD2828_W_Data(OLED.SigMode,CS_Master,(USB_Rx_Buffer[3]==0x00) ? RM295_Gamma_r[i] : ((USB_Rx_Buffer[3]==0x42) ? RM295_Gamma_g[i] : RM295_Gamma_b[i]));
                                    }
                                    else if(USB_Rx_Buffer[0]==RM67198)
                                    {
                                        SSD2828_W_Data(OLED.SigMode,CS_Master,(USB_Rx_Buffer[3]==0x00) ? RM295_Gamma_r[i] : ((USB_Rx_Buffer[3]==0x42) ? RM295_Gamma_g[i] : RM295_Gamma_b[i]));
                                    }

                                    if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA);                                              
                                    delay_ms(5);                                                          
                                    USB_Rx_Buffer[4+i]=SSD2828_R_Reg(OLED.SigMode,CS_Master,0xFF); 
                                }
                                delay_ms(1);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的 Gamma数据  ：ok          
                            }
                            break;
                            case 0x0C://otp time
                            {
                                RM195_otptime=USB_Rx_Buffer[4];
                                USB_Rx_Buffer[4] = Uart_Error_None;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 写Gamma状态  ：ok  
                            }  
                            break;                            
                            case 0x0A://otp page1
                            case 0x0D://OTP page0
                            case 0x0E://OTP page2
                            case 0x0F://OTP page3-5
                            {    
                                delay_ms(120); 
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data);
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x00FE);
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0x28);
                                delay_ms(200);  //otp
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x04FE);	
															//	SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x40FE);	
                                delay_ms(200);  

                                if(USB_Rx_Buffer[1]==0x0D)
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(RM195_otptime==0x01) ? 0x01F7 : ((RM195_otptime==0x02) ? 0x01F8 : 0x02F8));//OTP_cmd2_P0_PGM_EN
                                if(USB_Rx_Buffer[1]==0x0A)
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(RM195_otptime==0x01) ? 0x02F7 : ((RM195_otptime==0x02) ? 0x08F8 : 0x10F8) );//OTP_cmd2_P1_PGM_EN
                                if(USB_Rx_Buffer[1]==0x0E)
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(RM195_otptime==0x01) ? 0x04F7 : ((RM195_otptime==0x02) ? 0x20F8 : 0x40F8));//OTP_cmd2_P2_PGM_EN
                                if(USB_Rx_Buffer[1]==0x0F)
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,(RM195_otptime==0x01) ? 0x38F7 : 0x0000);//OTP_cmd2_P3-5_PGM_EN
             
                                delay_ms(200); 
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x03F2);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0xA5F3);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x5AF4);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x3CF5);                        
                                delay_ms(800);         // wait for >400 ms   
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);//read status bit
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC1,0x0002);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC0,0x0001);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xEF);
                                if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI   烧录OTP
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA);
                                delay_ms(100);															   								
                                tmp=SSD2828_R_Reg(OLED.SigMode,CS_Master,0xFF); 
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                if (tmp&0x02)       //出错
                                {
                                    USB_Rx_Buffer[4] = Uart_Error_Oth;
                                }
                                else                //正确
                                    USB_Rx_Buffer[4] = Uart_Error_None; 
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 写OTP状态  ：ok                                  
                            }
                            break;
                            case 0x0B://otp time read
                            {
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data);
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x04FE);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x03F6);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                SSD2828_W_Data(OLED.SigMode,CS_Master,0xEE);
                                if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA);							
                                delay_ms(50);
                                                                    
                                tmp=SSD2828_R_Reg(OLED.SigMode,CS_Master,0xFF); 	
                                if (tmp&0x10)
                                    RM195_otptime=3;
                                else if (tmp&0x08)
                                    RM195_otptime=2;
                                else
                                {
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                    SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                    SSD2828_W_Data(OLED.SigMode,CS_Master,0xED);
                                    if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI   
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA);							
                                    delay_ms(50);
                                
                                    tmp=SSD2828_R_Reg(OLED.SigMode,CS_Master,0xFF); 												
                                    if (tmp&0x02)
                                        RM195_otptime=1;
                                }			
                                USB_Rx_Buffer[4]=RM195_otptime;
                                RM195_otptime=0x00;
                                delay_ms(5);
                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                USB_Rx_Buffer[3] = Uart_Error_None;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的OTP数据  ：ok  
                            }
                            break;        
                            default:	
                                break;	
                            }
                                        
                    }
                    else if((USB_Rx_Buffer[0]==RM67160)||(USB_Rx_Buffer[0]==RM67162))
                    {
                        if(USB_Rx_Buffer[0]==RM67160)
                        {
                            gamma_read_num=22;
                            gamma_write_num=22;
                        }
                        else if(USB_Rx_Buffer[0]==RM67162)
                        {
                            gamma_read_num=50;
                            gamma_write_num=50;
                        }
                        switch (USB_Rx_Buffer[1]&0x0F)
                        {      

                            case 0x01:                                      //写寄存器
                            {
                                if((OLED.SigMode == SPI3_Mode)||(OLED.SigMode == SPI4_Mode))          //3线 4线8bit SPI 模式
                                {
                                    SPI_Write_code(OLED.SigMode,CS_Master,USB_Rx_Buffer[3],USB_Rx_Buffer[4]);  
                                }
                                else if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))
                                {
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
                                    delay_ms(5);
                                    SSD2828_W_Array(OLED.SigMode,CS_Master,USB_Rx_Buffer,2);
                                    delay_ms(50);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                }
                                USB_Rx_Buffer[4] = Uart_Error_None;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 写寄存器状态  ：ok  
                            }
                            break;
                            
                            case 0x02:                                      //读寄存器
                            {
                                if(OLED.SigMode == SPI3_Mode)          //3线8bit SPI 模式
                                {

                                }
                                else if(OLED.SigMode == SPI4_Mode)          //4线8bit SPI 模式
                                {
                                
                                }
                                else if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))
                                {
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);
                                    delay_ms(5);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC1,0x0002); 
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC0,0x0001); 
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);                                    
                                    for(i=0;i<3;i++)
                                    {
                                        SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                        SSD2828_W_Data(OLED.SigMode,CS_Master,USB_Rx_Buffer[3]);
                                        if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI   
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA);  
                                        delay_ms(15);
                                        USB_Rx_Buffer[4]=SSD2828_R_Reg(OLED.SigMode,CS_Master,0xFF);
                                    }                              
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                    delay_ms(5);
                                    STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的 寄存器数据  ：ok  	
                                }
                            }
                            break;
                            case 0x08:                                      //写Gamma
                            {
                                if((OLED.SigMode == SPI3_Mode)||(OLED.SigMode == SPI4_Mode))          //3线 4线8bit SPI 模式
                                {                                   
                                    if(USB_Rx_Buffer[1] == 0x08)
                                        SPI_Write_code(OLED.SigMode,CS_Master,0xFE,0x02);
                                    else if(USB_Rx_Buffer[1] == 0x18)
                                        SPI_Write_code(OLED.SigMode,CS_Master,0xFE,0x03);                                 
                                    for( i = 0;i<gamma_write_num;i++)
                                    {	                                                                   
                                        SPI_Write_code(OLED.SigMode,CS_Master,((USB_Rx_Buffer[3]==0x00) ? ((USB_Rx_Buffer[0]==RM67160) ? RM160_Gamma_r[i] : RM162_Gamma_r[i]) : \
                                                                              ((USB_Rx_Buffer[3]==0x34) ? ((USB_Rx_Buffer[0]==RM67160) ? RM160_Gamma_g[i] : RM162_Gamma_g[i]) : \
                                                                                                          ((USB_Rx_Buffer[0]==RM67160) ? RM160_Gamma_b[i] : RM162_Gamma_b[i]))),USB_Rx_Buffer[i+4]);     
                                        delay_ms(1);
                                    }                                    
                                }
                                else if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))
                                {
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
                                    delay_ms(5);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                    if(USB_Rx_Buffer[1] == 0x08)
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x02FE);
                                    else if(USB_Rx_Buffer[1] == 0x18)
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x03FE);
                                    for( i = 0;i<gamma_write_num;i++)
                                    {	                                                                   
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,  ((USB_Rx_Buffer[3]==0x00) ? ((USB_Rx_Buffer[i+4]<<8)+((USB_Rx_Buffer[0]==RM67160) ? RM160_Gamma_r[i] : RM162_Gamma_r[i])) : \
                                                                                    ((USB_Rx_Buffer[3]==0x34) ? ((USB_Rx_Buffer[i+4]<<8)+((USB_Rx_Buffer[0]==RM67160) ? RM160_Gamma_g[i] : RM162_Gamma_g[i])) : \
                                                                                                                ((USB_Rx_Buffer[i+4]<<8)+((USB_Rx_Buffer[0]==RM67160) ? RM160_Gamma_b[i] : RM162_Gamma_b[i])))));      //RM67160
                                        delay_ms(1);
                                    }	
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);                              
                                }
                                USB_Rx_Buffer[4] = Uart_Error_None;
                                USB_Rx_Buffer[1] =  0x08;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 写Gamma状态  ：ok  
                            }
                            break;
                            case 0x09:                                      //读Gamma
                            {
                                if(OLED.SigMode == SPI3_Mode)          //3线8bit SPI 模式
                                {
                             
                                }
                                else if(OLED.SigMode == SPI4_Mode)          //4线8bit SPI 模式
                                {
                                
                                }
                                else if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))
                                {
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);   

                                    if(USB_Rx_Buffer[0]==RM67160)
                                    {
                                        if(USB_Rx_Buffer[1] == 0x09)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x02FE);
                                        else if(USB_Rx_Buffer[1] == 0x19)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x03FE);  
                                    }
                                    else if(USB_Rx_Buffer[0]==RM67162)
                                    {
                                        if(USB_Rx_Buffer[1] == 0x09)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0xA2FE);
                                        else if(USB_Rx_Buffer[1] == 0x19)
                                            SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0xA3FE);  
                                    }
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC1,0x0002); 
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC0,0x0001); 
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                            	                        
                                    for( i = 0;i<gamma_read_num;i++)							
                                    {   
                                        for(j=0;j<3;j++)
                                        {

                                            SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                            SSD2828_W_Data(OLED.SigMode,CS_Master,(USB_Rx_Buffer[3]==0x00) ? ((USB_Rx_Buffer[0]==RM67160) ? RM160_Gamma_r[i] : RM162_Gamma_r[i]) : \
                                                                                  (USB_Rx_Buffer[3]==0x34) ? ((USB_Rx_Buffer[0]==RM67160) ? RM160_Gamma_g[i] : RM162_Gamma_g[i]) : \
                                                                                                             ((USB_Rx_Buffer[0]==RM67160) ? RM160_Gamma_b[i] : RM162_Gamma_b[i]));

                                            if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI
                                                SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA); 
                                            delay_ms(5);
                                            USB_Rx_Buffer[i+4]=SSD2828_R_Reg(OLED.SigMode,CS_Master,0xFF);
                                        }

                                    }                   
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);	                              
                                }
                                USB_Rx_Buffer[1] = (USB_Rx_Buffer[0]==RM67160) ? 0x09 : 0x0C;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的 Gamma数据  ：ok  
                            }
                            break;
                            case 0x0A:								//OTP Start
                            {
                                if((OLED.SigMode == SPI3_Mode)||(OLED.SigMode == SPI4_Mode))          //3线 4线8bit SPI 模式
                                {
                                    delay_ms(120); 
                                    SPI_Write_code(OLED.SigMode,CS_Master,0xfe,0x00);
                                    SPI_Write_u8_Cmd(OLED.SigMode,CS_Master,0x28); 
                                    delay_ms(100);  //otp
                                    SPI_Write_code(OLED.SigMode,CS_Master,0xfe,0x01);
                                    SPI_Write_code(OLED.SigMode,CS_Master,0xf0,0x10);
                                    SPI_Write_code(OLED.SigMode,CS_Master,0xf2,0x03);
                                    SPI_Write_code(OLED.SigMode,CS_Master,0xf3,0xA5); 
                                    SPI_Write_code(OLED.SigMode,CS_Master,0xf4,0x5a);
                                    SPI_Write_code(OLED.SigMode,CS_Master,0xf5,0x3c);
                                    delay_ms(500);         // wait for >400 ms 
                                    
                                    
                                    USB_Rx_Buffer[4] = Uart_Error_None;
                                    STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 OTP状态  ：ok
                                }
                                else if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))
                                {
                                    delay_ms(120); 
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data);
                                    delay_ms(5);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x00FE);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                    SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                    SSD2828_W_Data(OLED.SigMode,CS_Master,0x28);
                                    delay_ms(200);  //otp
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x01FE);
                                    SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                    SSD2828_W_Data(OLED.SigMode,CS_Master,0xF0);                                    

                                    if(USB_Rx_Buffer[1] == 0x0A)
                                    {
                                        SSD2828_W_Data(OLED.SigMode,CS_Master,0x10); 
                                        
                                    }
                                    else if(USB_Rx_Buffer[1] == 0x1A)
                                    {
                                        SSD2828_W_Data(OLED.SigMode,CS_Master,0x08); 

                                    }                                        


                                    delay_ms(200); 
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x03F2);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0xA5F3);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x5AF4);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x3CF5); 
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                    delay_ms(800);         // wait for >400 ms   
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);//read status bit
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC1,0x0002);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xC0,0x0001);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                    SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                    SSD2828_W_Data(OLED.SigMode,CS_Master,0xEF);
                                    if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI   烧录OTP
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA);
                                    delay_ms(100);															   								
                                    tmp=SSD2828_R_Reg(OLED.SigMode,CS_Master,0xFF); 
                                    delay_ms(5);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                    if (tmp&0x02)       //出错
                                    {
                                        USB_Rx_Buffer[4] = Uart_Error_Oth;
                                    }
                                    else                //正确
                                        USB_Rx_Buffer[4] = Uart_Error_None; 
                                    STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 OTP状态  ：ok                                       
                                }
                            }
                            break;
                            case 0x0B:                                      //Read OTP Time
                            {
                                if(OLED.SigMode == SPI3_Mode)          //3线8bit SPI 模式
                                {
                             
                                }
                                else if(OLED.SigMode == SPI4_Mode)          //4线8bit SPI 模式
                                {
                                
                                }
                                else if((OLED.SigMode == Mipi_Mode)||(OLED.SigMode == CMD_Mode))
                                {
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data);
                                    delay_ms(5);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0002);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,0x0000);
                                    
                                    if(USB_Rx_Buffer[0]==RM67160)
                                    {
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0x01FE); 
                                    }
                                    else if(USB_Rx_Buffer[0]==RM67162)
                                    {
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBF,0xA1FE);   
                                    } 
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,LP_B7_Data|0x0080);
                                    delay_ms(5);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,0x0001);
                                    SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                                    SSD2828_W_Data(OLED.SigMode,CS_Master,0xEE);
                                    if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI  
                                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xD4,0x00FA);							
                                    delay_ms(50);                                 
                                    tmp=SSD2828_R_Reg(OLED.SigMode,CS_Master,0xFF); 
                                    
                                    if (tmp&0x10)
                                        USB_Rx_Buffer[4] = 1;
                                    else if (tmp&0x08)
                                        USB_Rx_Buffer[4] = 2;
                                    else
                                        USB_Rx_Buffer[4] = 0;
                                    delay_ms(5);
                                    SSD2828_W_Reg(OLED.SigMode,CS_Master,0xB7,HS_B7_Data);
                                    USB_Rx_Buffer[3] = Uart_Error_None;
                                    STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 读取的OTP数据  ：ok                                  
                                }
                                USB_Rx_Buffer[4] = Uart_Error_None;
                                STM2PC_RM671xx(pdev,CDC_IN_EP,USB_Rx_Buffer,USB_Rx_Buffer[2]+3);           //返回 OTP状态  ：ok
                            }
                            break;

                        }
                    }
                }                 
            }           
            else
            {                    
                if(NUM==0)
                {
                    NUM = 1; 
                    if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI
                    {
#if DSI_Set_Window_EN == 1                    
                        SSD2828_Set_Window(OLED.SigMode,CS_Master,(OLED.H_pixel==390) ? 4 : 0,(OLED.H_pixel==390) ? OLED.H_pixel+4 : OLED.H_pixel,0,OLED.V_pixel);
#endif                   
                        ICS307_HighLow_Set(OLED.pixel_clk,ICS307_Buf_Data);
                        if(Valid_BMP_No==0)
                        {
                            Valid_BMP_No=1;
                        }
                        else
                        {
                            Valid_BMP_No=0;
                        }
                        write_cmd(0x3456);						//将OLED时序数据发送到FPGA
                        delay_us(2);
                        write_cmd((u16)(((Valid_BMP_No*OLED.V_pixel*OLED.H_pixel)*8/5)>>16));				//	0:H_PIXEL	1:H_B	2:H_F	3:H_S	4:高8(单双6,8)
                        delay_us(2);
                        write_cmd((u16)((Valid_BMP_No*OLED.V_pixel*OLED.H_pixel)*8/5));						//	0:V_PIXEL	1:V_B	2:V_F	3:V_S	4:无效
                        delay_us(2);
                        write_cmd(0x6543);
                                
                        delay_ms(10);         //如果出现不正常的线条，可加大延时
                    }
                    else if(OLED.SigMode == CMD_Mode)  //Command
                    {
                        
#if DSI_Set_Window_EN == 1                    
                        SSD2828_Set_Window(OLED.SigMode,CS_Master,(OLED.H_pixel==390) ? 4 : 0,(OLED.H_pixel==390) ? OLED.H_pixel+4 : OLED.H_pixel,0,OLED.V_pixel);
#endif
                        
                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBC,(u16) (OLED.H_pixel*OLED.V_pixel*3)); 
                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBD,(u16)((OLED.H_pixel*OLED.V_pixel*3)>>16)); 
                        SSD2828_W_Reg(OLED.SigMode,CS_Master,0xBE,(u16) (OLED.H_pixel*3));                         
                        SSD2828_W_Cmd(OLED.SigMode,CS_Master,0xBF);
                        SSD2828_W_Cmd(OLED.SigMode,CS_Master,0X2C);
                    }
                    else if((OLED.SigMode == SPI3_Mode)||(OLED.SigMode == SPI4_Mode))          //3线8bit SPI 模式   //4线8bit SPI 模式
                    {
#if SPI_Set_Window_EN == 1                       
                        SPI_Set_Window  (OLED.SigMode,CS_Master,0, OLED.H_pixel-1,0, OLED.V_pixel-1);
#endif
                        SPI_Write_code  (OLED.SigMode,CS_Master ,0xFE,0x01);
                        SPI_Write_code  (OLED.SigMode,CS_Master ,0x04,0x80);
                        SPI_Write_code  (OLED.SigMode,CS_Master ,0xFE,0x00);
                        SPI_Write_code  (OLED.SigMode,CS_Master ,0xC4,0x31);        // SPI3-SP3T 2Wire
                    }    
                }
                for(i=0;i<USB_ReceivedCount;i++)
                    buffer[i+VCP_Receive_True_num] = USB_Rx_Buffer[i];
                
                VCP_Receive_True_num+=USB_ReceivedCount;
                
                if(VCP_Receive_True_num == OLED.H_pixel*3)
                {     
                    if(OLED.SigMode == Mipi_Mode)  //RGB+SPI= MIPI
                    {
                        SSD2828_W_RAM_buffer_8bits_part(OLED.pixel_clk,OLED.SigMode,buffer,OLED.H_pixel,1,OLED.DisMode);
                    }
                    else if(OLED.SigMode == CMD_Mode)  //Command
                    {
                        SPI_CS_Select(CS_Master,0);
                        SSD2828_W_RAM_buffer_8bits_part(OLED.pixel_clk,OLED.SigMode,buffer,OLED.H_pixel,1,0);
                        SPI_CS_Select(CS_Master,1);
                    }
                    else if((OLED.SigMode == SPI3_Mode)||(OLED.SigMode == SPI4_Mode))          //3线8bit SPI 模式   //4线8bit SPI 模式
                    {    
                        if(Rec_first_line_Flag == 1)
                            Rec_first_line_Flag = 0;
                        SPI_WriteRAM_buffer_part(OLED.SigMode,CS_Master,buffer,OLED.H_pixel,1,Rec_first_line_Flag);
                    } 
                    VCP_Receive_True_num=0;
                }	
            }        
        } 
        else if((USB_ReceivedCount==1) && (USB_Rx_Buffer[0]==0X5A) &&(NUM == 1))  //图片接受完成标志
        {            
            NUM=0;
            if(OLED.SigMode == Mipi_Mode)
            {
                Load_done_HIGH;
                FSMC_BMP_NUM(Valid_BMP_No);                     //当前画面序号
                ICS307_ValidClk_Set(ICS307_Valid_Data); 
            }
        }        
	}
    return USBD_OK;
}

/**
  * @brief  usbd_audio_SOF
  *         Start Of Frame event management
  * @param  pdev: instance
  * @param  epnum: endpoint number
  * @retval status
  */
static uint8_t  usbd_cdc_SOF (void *pdev)
{      
  static uint32_t FrameCount = 0;
  
  if (FrameCount++ == CDC_IN_FRAME_INTERVAL)
  {
    /* Reset the frame counter */
    FrameCount = 0;
    
    /* Check the data to be sent through IN pipe */
    Handle_USBAsynchXfer(pdev);
  }
  
  return USBD_OK;
}

/**
  * @brief  Handle_USBAsynchXfer
  *         Send data to USB
  * @param  pdev: instance
  * @retval None
  */
static void Handle_USBAsynchXfer (void *pdev)
{
  uint16_t USB_Tx_ptr;
  uint16_t USB_Tx_length;
  
  if(USB_Tx_State != 1)
  {
      //当APP_Rx_ptr_out 达到 APP_RX_DATA_SIZE 时，将其置0，
      //也就是在循环缓冲区中绕了一圈回到缓冲区起始地址。
    if (APP_Rx_ptr_out == APP_RX_DATA_SIZE)
    {
      APP_Rx_ptr_out = 0;
    }
     //当 APP_Rx_prt_out 赶上 APP_Rx_ptr_in 时，证明 Buffer 里边的数据已经发送完毕，返回   
    if(APP_Rx_ptr_out == APP_Rx_ptr_in)     //Buffer 里边的数据已经发送完毕
    {
      USB_Tx_State = 0; 
      return;
    }
    
    if(APP_Rx_ptr_out > APP_Rx_ptr_in) /* rollback */
    { 
      APP_Rx_length = APP_RX_DATA_SIZE - APP_Rx_ptr_out;
    
    }
    else 
    {
      APP_Rx_length = APP_Rx_ptr_in - APP_Rx_ptr_out;
     
    }
#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
     APP_Rx_length &= ~0x03;
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
    
    if (APP_Rx_length > CDC_DATA_IN_PACKET_SIZE)
    {
      USB_Tx_ptr = APP_Rx_ptr_out;
      USB_Tx_length = CDC_DATA_IN_PACKET_SIZE;
      
      APP_Rx_ptr_out += CDC_DATA_IN_PACKET_SIZE;	
      APP_Rx_length -= CDC_DATA_IN_PACKET_SIZE;
    }
    else
    {
      USB_Tx_ptr = APP_Rx_ptr_out;
      USB_Tx_length = APP_Rx_length;
      
      APP_Rx_ptr_out += APP_Rx_length;
      APP_Rx_length = 0;
    }
    USB_Tx_State = 1; 

    DCD_EP_Tx (pdev,
               CDC_IN_EP,
               (uint8_t*)&APP_Rx_Buffer[USB_Tx_ptr],
               USB_Tx_length);
  }  
  
}

/**
  * @brief  USBD_cdc_GetCfgDesc 
  *         Return configuration descriptor
  * @param  speed : current device speed
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
static uint8_t  *USBD_cdc_GetCfgDesc (uint8_t speed, uint16_t *length)
{
  *length = sizeof (usbd_cdc_CfgDesc);
  return usbd_cdc_CfgDesc;
}

/**
  * @brief  USBD_cdc_GetCfgDesc 
  *         Return configuration descriptor
  * @param  speed : current device speed
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
#ifdef USE_USB_OTG_HS 
static uint8_t  *USBD_cdc_GetOtherCfgDesc (uint8_t speed, uint16_t *length)
{
  *length = sizeof (usbd_cdc_OtherCfgDesc);
  return usbd_cdc_OtherCfgDesc;
}
#endif
/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
