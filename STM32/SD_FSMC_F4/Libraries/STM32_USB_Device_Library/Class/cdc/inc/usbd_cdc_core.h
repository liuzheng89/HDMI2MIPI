/**
  ******************************************************************************
  * @file    usbd_cdc_core.h
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    22-July-2011
  * @brief   header file for the usbd_cdc_core.c file.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/

#ifndef __USB_CDC_CORE_H_
#define __USB_CDC_CORE_H_

#include  "usbd_ioreq.h"
#include "main.h"
//#define   CS_Master        CS1
//#define   CS_Slaver        CS2
#ifdef STM32F2XX
#define InsertBlack     1
#define SSD_Command_8
#define DSI_Set_Window_EN   0       //0:Disable  1:Enable
#define SPI_Set_Window_EN   1       //0:Disable  1:Enable
#else
#define InsertBlack     0
#define SSD_Command_16
#define DSI_Set_Window_EN   0       //0:Disable  1:Enable
#define SPI_Set_Window_EN   1       //0:Disable  1:Enable
#endif
#define BMP_R_Byte_Max  2560*3
#define SSD2828_Code_Max				32

#define IniCode_Max		          255
#define IniCode_table_num				50

typedef struct {
	u16 H_pixel;
	u8	H_Back;
	u8	H_Front;
	u8	H_Sync;
	u16 V_pixel;
	u8	V_Back;
	u8	V_Front;
	u8	V_Sync;
	u8 DisMode;	//0:逐点扫描，1：左右分屏扫描，2：上下分屏扫描
	u8 SigMode;	//0x:MCU      1x:MIPI    2x:SPI3     3x:SPI4
	u8 RGBMode;
	float pixel_clk;

} OLED_Para;

extern u32 WriteAddress; //Base address of Sector 2
extern u8  buffer[BMP_R_Byte_Max];
extern u32 buffer1[BMP_R_Byte_Max-1];
extern u32 i,k;
extern u32 j;
extern u8 Valid_BMP_No;
extern u8	Uart_rx_flag;
extern u32 File_length;
extern OLED_Para OLED;
extern __IO  uint32_t  USB_ReceivedCount;
extern uint8_t USB_Rx_Buffer   [CDC_DATA_MAX_PACKET_SIZE];
extern u8 NUM;
extern u8 SendTo_PC_cfgFile;
extern __IO  uint8_t USB_StatusDataSended;
extern u32 ICS307_Buf_Data;
extern u32 ICS307_Buf_Data1;
extern u32 ICS307_Valid_Data;
extern u8 PatternR,PatternG,PatternB;
extern u8 Pure_Color_Flag;
extern u8  Rec_first_line_Flag;
extern u32 SSD28xx_Code[SSD2828_Code_Max];                         //存放 SSD2828配置 数据
extern u32 Timing_and_InitCode[IniCode_Max];                       //更新时 临时存放Timing和Driver IC初始化代码
extern u8  SSD28xxCode_count;                                  //SSD2828配置代码的 行数
extern u8  InitCode_valid[IniCode_table_num][IniCode_Max];         //存放 Driver IC初始化代码
extern u16  InitCode_count;                                     //DriverIC初始化代码的 行数
extern u32 Code_length;
extern u32 bw;
extern u8  Receive_BIN_Flag;
extern u8  Receive_BIN_Flag1;
extern u32 VCP_Receive_True_num;
extern  u32 USB_Rx_Demura;
extern u32 Receive_BIN_Length;

extern u32  Device_ID_Buf[2];
extern u32  Device_ID;
extern u32  Device_Addr;

#define Uart_Error_None					0x00
#define Uart_Error_Oth					0x02
#define Uart_Error_IntFlash				0x03        //flash内部没有 配置文件 错误

#define REC_LCDCONF_START				0x0A
#define REC_LCDCONF_NUM					25
#define REC_W_LCDCONF_FLAG				0x01
#define REC_W_DISP_WINDOW_CONF_FLAG				0x11
#define REC_R_LCDCONF_FLAG				0x02
#define REC_W_PATTERN_FLAG				0x07		//写入Pattern参数
#define REC_R_PATTERN_FLAG				0x08		//读取Pattern参数
#define REC_R_SW_VERSION_FLAG			0x0D
#define REC_W_DeviceID_FLAG       0x0E
#define REC_UPDATABIN_Start				0x55

#define REC_SSD_CODE_START				0x0D
#define REC_W_SSD_FLAG				    0x01
#define REC_W_SSD_ONE_FLAG				0x03
#define REC_W_CODE_FLAG				    0x02
#define REC_W_CODE_ONE_FLAG				0x04
#define REC_R_CODE_FLAG				    0x07
    

#define DE_MURA							0x2F
#define GenWR             	0x99

#define Rec_R_Config_Start				0x0B
#define Rec_R_Config_flag				0x01
/** @addtogroup STM32_USB_OTG_DEVICE_LIBRARY
  * @{
  */
  
/** @defgroup usbd_cdc
  * @brief This file is the Header file for USBD_cdc.c
  * @{
  */ 


/** @defgroup usbd_cdc_Exported_Defines
  * @{
  */ 
#define USB_CDC_CONFIG_DESC_SIZ                (67)
#define USB_CDC_DESC_SIZ                       (67-9)

#define CDC_DESCRIPTOR_TYPE                     0x21

#define DEVICE_CLASS_CDC                        0x02
#define DEVICE_SUBCLASS_CDC                     0x00


#define USB_DEVICE_DESCRIPTOR_TYPE              0x01
#define USB_CONFIGURATION_DESCRIPTOR_TYPE       0x02
#define USB_STRING_DESCRIPTOR_TYPE              0x03
#define USB_INTERFACE_DESCRIPTOR_TYPE           0x04
#define USB_ENDPOINT_DESCRIPTOR_TYPE            0x05

#define STANDARD_ENDPOINT_DESC_SIZE             0x09

#define CDC_DATA_IN_PACKET_SIZE                 *(uint16_t *)(((USB_OTG_CORE_HANDLE *)pdev)->dev.pConfig_descriptor + 57)
        
#define CDC_DATA_OUT_PACKET_SIZE                *(uint16_t *)(((USB_OTG_CORE_HANDLE *)pdev)->dev.pConfig_descriptor + 64)

/*---------------------------------------------------------------------*/
/*  CDC definitions                                                    */
/*---------------------------------------------------------------------*/

/**************************************************/
/* CDC Requests                                   */
/**************************************************/
#define SEND_ENCAPSULATED_COMMAND               0x00
#define GET_ENCAPSULATED_RESPONSE               0x01
#define SET_COMM_FEATURE                        0x02
#define GET_COMM_FEATURE                        0x03
#define CLEAR_COMM_FEATURE                      0x04
#define SET_LINE_CODING                         0x20
#define GET_LINE_CODING                         0x21
#define SET_CONTROL_LINE_STATE                  0x22
#define SEND_BREAK                              0x23
#define NO_CMD                                  0xFF

/**
  * @}
  */ 


/** @defgroup USBD_CORE_Exported_TypesDefinitions
  * @{
  */
typedef struct _CDC_IF_PROP
{
  uint16_t (*pIf_Init)     (void);   
  uint16_t (*pIf_DeInit)   (void);   
  uint16_t (*pIf_Ctrl)     (uint32_t Cmd, uint8_t* Buf, uint32_t Len);
  uint16_t (*pIf_DataTx)   (uint8_t* Buf, uint32_t Len);
  uint16_t (*pIf_DataRx)   (uint8_t* Buf, uint32_t Len);
}
CDC_IF_Prop_TypeDef;
/**
  * @}
  */ 



/** @defgroup USBD_CORE_Exported_Macros
  * @{
  */ 
  
/**
  * @}
  */ 

/** @defgroup USBD_CORE_Exported_Variables
  * @{
  */ 
void SSD2828_signal_on(u8 signal_mode,u8 channel);
void SSD2828_initial_on(u8 signal_mode,u8 channel);
void Driver_ic_initial_on(u8 signal_mode,u8 channel);

void SPI_signal_on(u8 SPI_Mode,u8 channel);
extern USBD_Class_cb_TypeDef  USBD_CDC_cb;
/**
  * @}
  */ 

/** @defgroup USB_CORE_Exported_Functions
  * @{
  */
/**
  * @}
  */ 

#endif  // __USB_CDC_CORE_H_
/**
  * @}
  */ 

/**
  * @}
  */ 
  
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
