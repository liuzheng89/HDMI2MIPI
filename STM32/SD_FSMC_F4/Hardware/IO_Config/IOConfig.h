#ifndef _IOCONFIG_H
#define _IOCONFIG_H

#include "stm32fxxx.h"
#include "main.h"
//#define RCC_LEDPeriph_GPIO	RCC_AHB1Periph_GPIOA
//#define LED1								GPIO_Pin_1
//#define LEDPeriph_GPIO			GPIOA

//#define RCC_KEYPeriph_GPIO	RCC_AHB1Periph_GPIOB
//#define KEY1								GPIO_Pin_2
//#define KEYPeriph_GPIO			GPIOB

#define RCC_LEDPeriph_GPIO				RCC_AHB1Periph_GPIOC
#define LED1							GPIO_Pin_13
#define LED2							GPIO_Pin_14
#define LEDPeriph_GPIO					GPIOC

#define LED_HIGH						GPIO_SetBits	( LEDPeriph_GPIO,  LED1)
#define LED_LOW							GPIO_ResetBits( LEDPeriph_GPIO,  LED1)

#define LED2_HIGH						GPIO_SetBits	( LEDPeriph_GPIO,  LED2)
#define LED2_LOW							GPIO_ResetBits( LEDPeriph_GPIO,  LED2)

#define RCC_KEYPeriph_GPIO					RCC_AHB1Periph_GPIOF
#define KEY1								GPIO_Pin_8
#define KEY2								GPIO_Pin_9
#define KEYPeriph_GPIO						GPIOF

#define RCC_KEY3Periph_GPIO					RCC_AHB1Periph_GPIOE
#define KEY3								GPIO_Pin_2
#define KEY3Periph_GPIO						GPIOE

#define RCC_KEY4Periph_GPIO					RCC_AHB1Periph_GPIOB
#define KEY4								GPIO_Pin_4
#define KEY4Periph_GPIO						GPIOB

#define RCC_SSD_RESETPeriph_GPIO		RCC_AHB1Periph_GPIOD
#define SSD_RESET						GPIO_Pin_3
#define SSD_RESETPeriph_GPIO			GPIOD

#define RCC_USB_RESETPeriph_GPIO		RCC_AHB1Periph_GPIOC
#define USB_RESET						GPIO_Pin_1
#define USB_RESETPeriph_GPIO			GPIOC

#define RCC_Load_donePeriph_GPIO		RCC_AHB1Periph_GPIOC		//ARM_FPAG_4
#define Load_done						GPIO_Pin_4
#define Load_donePeriph_GPIO			GPIOC

#define RCC_FPGA_ProPeriph_GPIO		RCC_AHB1Periph_GPIOA		//ARM_FPAG_4
#define FPGA_Pro						GPIO_Pin_0
#define FPGA_ProPeriph_GPIO			GPIOA


#define RCC_FPGA_ResetPeriph_GPIO		RCC_AHB1Periph_GPIOC		//ARM_FPAG_5
#define FPGA_Reset						GPIO_Pin_5
#define FPGA_ResetPeriph_GPIO			GPIOC

#define Load_done_HIGH					GPIO_SetBits	( Load_donePeriph_GPIO,  Load_done)
#define Load_done_LOW					GPIO_ResetBits( Load_donePeriph_GPIO,  Load_done)

#define SSD_RESET_HIGH					GPIO_SetBits	( SSD_RESETPeriph_GPIO,	SSD_RESET)
#define SSD_RESET_LOW					GPIO_ResetBits( SSD_RESETPeriph_GPIO,	SSD_RESET)

#define USB_RESET_HIGH					GPIO_SetBits	( USB_RESETPeriph_GPIO,	USB_RESET)
#define USB_RESET_LOW					GPIO_ResetBits( USB_RESETPeriph_GPIO,	USB_RESET)

#define FPGA_RESET_HIGH					GPIO_SetBits	( FPGA_ResetPeriph_GPIO,	FPGA_Reset)
#define FPGA_RESET_LOW					GPIO_ResetBits( FPGA_ResetPeriph_GPIO,	FPGA_Reset)


#define RCC_IF_Sel_GPIO		            RCC_AHB1Periph_GPIOG		//ARM_FPAG_5
#define IF_Sel2						GPIO_Pin_13
#define IF_Sel1						GPIO_Pin_14
#define IF_SelPeriph_GPIO			GPIOG

#define IF_Sel2_Cmd					    GPIO_SetBits	( IF_SelPeriph_GPIO,  IF_Sel2)
#define IF_Sel2_Video				    GPIO_ResetBits( IF_SelPeriph_GPIO,  IF_Sel2)

#define IF_Sel1_Cmd					    GPIO_SetBits	( IF_SelPeriph_GPIO,  IF_Sel1)
#define IF_Sel1_Video					GPIO_ResetBits( IF_SelPeriph_GPIO,  IF_Sel1)




void LED_IO_init(void);
void IF_Sel_init(void);
void FPGA_Pro_init(void);
void BUTTON_IO_init(void);
void EXTIInit(void);
void EXIT_Enable(u8 status);
void Other_IO_init(void);
void FPGA_ResetProcess(void);
void HardWare_Init(void);

#endif

