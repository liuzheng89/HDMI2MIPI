#ifndef __SPI_INIT2_H
#define __SPI_INIT2_H

#include <stm32f2xx.h>

void SPI2_Config(void);
u8 SPI2_ReadWriteByte(u8 TxData);

#endif


