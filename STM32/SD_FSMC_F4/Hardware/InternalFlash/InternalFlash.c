#include "InternalFlash.h"

/**************************************************
功能：将数据字写入到内部FLAH里
参数：
			FlashAddress：准备写入的起始地址
			Data	：准备写入的数据字(宽度为32位)
			DataLength	：	需要写入的数据字的长度
返回：无
****************************************************/

uint32_t FLASH_If_Write(uint32_t* FlashAddress, uint32_t* Data,uint16_t DataLength)
{
	uint32_t i = 0;
	for (i = 0; i < DataLength; i++)
	{
		/* 设备电压范围支持：2.7-3.6V，通过 字 操作 */
		if (FLASH_ProgramWord(*FlashAddress, *(uint32_t*)(Data+i)) ==FLASH_COMPLETE)
		{
			/* 检查写入的值*/
			if (*(uint32_t*)*FlashAddress != *(uint32_t*)(Data+i))
			{
				/* 内容不匹配*/
				return(2);
			}
			/* 增加FLASH的目标地址*/
			*FlashAddress += 4;
		}
		else
		{
			/*在写FLASH的过程中发生了错误*/
			return (1);
		}
	}
	return (0);
}
/**************************************************
功能：从内部FLAH读取数据字到数组里
参数：
			FlashAddress：读取的起始地址
			Data	：读取的数据字保存的数组(宽度为32位)
			start_add	：从数组的第几个位置开始保存
			DataLength	：	需要读取的数据字的长度
返回：无

****************************************************/
void FLASH_If_Read(uint32_t* FlashAddress, uint32_t* Data, u8 start_add ,uint16_t DataLength)
{
	uint32_t i = 0;
	Data+=start_add;
	for (i = 0; i < DataLength; i++)
	{
		*Data=*(uint32_t*)*FlashAddress;
		Data++;
		*FlashAddress += 4;
	}
}
