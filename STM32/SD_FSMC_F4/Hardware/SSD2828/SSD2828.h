#ifndef SSD2828_H
#define SSD2828_H

#include "SPI_INIT.h" 
#include "IOConfig.h"
#include "stm32fxxx.h"
#include "delay.h"
#include "fsmc.h"

void Set_2828_reset(u8 Sig_Mode);
void SSD2828_W_Cmd(u8 SigMode ,u8 channel,u8 cmd);
void SSD2828_W_Cmd_NOCS(u8 cmd);
void SSD2828_W_Data(u8 SigMode ,u8 channel,u16 Value);
u16   SSD2828_R_Data(u8 SigMode ,u8 channel);
void SSD2828_W_Reg(u8 SigMode ,u8 channel,u8 Reg, u16 Value);
u16  SSD2828_R_Reg(u8 SigMode ,u8 channel,u8 Reg);
void SSD2828_W_Array(u8 SigMode ,u8 channel,u8 para[],u8 offerset);
void SSD2828_W_RAM_buffer_8bits_part(float pclk,u8 SigMode ,u8* buffer ,u16 length,u8  multiple ,u8 DisMode);
void SSD2828_W_RAM_Pic(float pclk,u8 SigMode ,u8 channel ,u16 H_Pix,u16 V_Pix,u8 R ,u8 G,u8  B );
void SSD2828_Set_Window(u8 SigMode ,u8 channel,u16 H_Start,u16 H_End,u16 V_Start,u16 V_End);
# endif

