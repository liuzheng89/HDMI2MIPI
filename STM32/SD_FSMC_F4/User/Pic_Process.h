#ifndef _PICPROCESS_H
#define _PICPROCESS_H


#include "stm32fxxx.h"
#include <stdio.h>
#include <string.h>
#include "diskio.h"
#include "ff.h"

#define File_NAME_MAX	32
#define File_NUM_MAX	32

extern char FileNames [File_NUM_MAX][File_NAME_MAX];
extern u8 PicCnt;

extern FILE *fout;
extern u32 br;
FRESULT scan_files(u8 Disc_type ,u8 status);        /* Start node to be scanned (also used as work area) */
#endif



