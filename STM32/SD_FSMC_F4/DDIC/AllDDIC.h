#ifndef _AllDDIC_H_
#define _AllDDIC_H_

#include  "usbd_cdc_core.h"
#include <ctype.h>                      /* Character functions                */
//------------------------------------------------------------------------------

#define DE_MURA			0x2F
#define RM67120				0x08
#define RM67195				0x09
#define RM67295				0x19
#define RM67160				0x0E
#define RM67162				0x1E 
#define RM67198				0x29
#define RM67298				0X2E
#define NT37710				0x2A
#define HX83200				0x2B
#define RM69300				0x2C
#define ICN9608				0x2D
#define RM69350             0x3C
#define TC1100				0x31
#define RM6D010				0x3D
#define UD61720				0x4C
#define CH13721				0x4A
//-------------------------------------------------------------------------------
#define ICName				0x3E  //由此向下编号，不要重复
#define R66455				0x30
#define HX83201A			0x51
#define FT2711				0X4B
#define VTDR6100			0X4D

//-----命名规则：ProcessForXX ,其中XX为当前IC预定义的编号------------------
void ProcessForIc51( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data);
void ProcessForIc30( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data);
void ProcessForIc4B( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data);
void ProcessForIc4D( USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data);


void ProcessForIc3E( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc3F( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc40( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc41( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc42( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc43( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc44( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc45( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc46( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc47( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc48( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc49( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用

void ProcessForIc4E( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc4F( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用
void ProcessForIc50( u8 CS_Master,u8 * USB_Rx_Buffer,u16 LP_B7_Data,u16 HS_B7_Data);//供后续自行修改用

























































































































































































































































































































//--------------------------------------------------------------------------------------
#endif

