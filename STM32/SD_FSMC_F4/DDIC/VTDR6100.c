//#include "debug.h"
#include "AllDDIC.h"

/********************************************************************************************
********************************************************************************************
    VTDR6100  写寄存器 读寄存器  写Gamma 读Gamma OTP烧录
********************************************************************************************
********************************************************************************************/
u8 vtdr_bufferR[50];
u8 vtdr_bufferG[50];
u8 vtdr_bufferB[50];

void VTDR6100_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
//	if(USB_Rx_Buffer[1]&0x80)
//	SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
//	else
//	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFfd);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写寄存器状态  ：ok  
}
void VTDR6100_Write_51Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	//SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFfd);
	delay_ms(5);
    SSD2828_W_Array(SigMode,channel,buffer,2);                               

    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写寄存器状态  ：ok  
}
void VTDR6100_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u16 tmp;
    buffer[1]=0x03;
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,buffer[2]); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer[0] = 0x01;
    buffer[1] = buffer[3];                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
    SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
    for(i=0;i<buffer[2]-1;i++)
    {
        SSD2828_W_Cmd(SigMode,channel,0xFA);		
        tmp=SPI3_Read_u16_Data(channel);
        buffer[4+i]=tmp>>8;
        buffer[5+i]=tmp;
        delay_ms(5);
        i++;
    }
	buffer[0]=0x4D;	
	buffer[1]=0x02;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 读取的 寄存器数据  ：ok 
}

void VTDR6100_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u8 i,j,k=0,temp;
	u8 m = 0x00;
	if((buffer[3]&0xFF)==0x00)
	{
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
			   vtdr_bufferR[i]=buffer[i+5];	    
		}
			 
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
			  vtdr_bufferR[i+16]=buffer[i+21]; 
		}
	
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
			   vtdr_bufferR[i+32]=buffer[i+37]; 
		}
			 
		for( i = 0;i<2;i++)   //1个绑点
		{	                                                                                  
			   vtdr_bufferR[i+48]=buffer[i+53];  
		}
	}
	else if((buffer[3]&0xFF)==0x42)
	{
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
		  vtdr_bufferG[i]=buffer[i+5];
		} 
		 
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
		  vtdr_bufferG[i+16]=buffer[i+21]; 
		} 
		 
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
		   vtdr_bufferG[i+32]=buffer[i+37]; 
		}
		 
		for( i = 0;i<2;i++)   //1个绑点
		{	                                                                                  
		   vtdr_bufferG[i+48]=buffer[i+53]; 
		}
	}
	else if((buffer[3]&0xFF)==0x81)
	{
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
			   vtdr_bufferB[i]=buffer[i+5];	
		} 
			 
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
			  vtdr_bufferB[i+16]=buffer[i+21];  
		}
			 
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
			   vtdr_bufferB[i+32]=buffer[i+37]; 
		}
			 
		for( i = 0;i<2;i++)   //1个绑点
		{	                                                                                  
			   vtdr_bufferB[i+48]=buffer[i+53];  
		}
	}
		
	//SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);
	//SSD2828_W_Reg(SigMode,channel,0xB7,0x035b);  //09->0b ,解决进高速闪屏
	delay_ms(5);
	buffer1[0] = 0x03;        
    buffer1[1] = 0xF0;
    buffer1[2] = 0xAA;
	buffer1[3] = 0x13;
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(5); 
	
	buffer1[0] = 0x02;        
    buffer1[1] = 0xC4;
    buffer1[2] = 0x01;
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(5); 
		
//	  for( j = 0;j<9;j++)   //循环9次
//	   {	                                                                                  
			   
			buffer1[0] = 0x02;        
			buffer1[1] = 0xBF;
//        	buffer1[2] = m;
		    buffer1[2] = 0x03;
			SSD2828_W_Array(SigMode,channel,buffer1,0);		 
				 
			SSD2828_W_Reg(SigMode,channel,0xBC,17); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB0); //GAMMA RED GROUP1
			for(i=0; i<16; i++)
			{
				SSD2828_W_Data(SigMode,channel,vtdr_bufferR[i]);	
			}
				 
			SSD2828_W_Reg(SigMode,channel,0xBC,17); 
			SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB1); //GAMMA RED GROUP2
			for(i=0; i<16; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferR[i+16]);	
			}
								 
			SSD2828_W_Reg(SigMode,channel,0xBC,17); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB2); //GAMMA RED GROUP3
			for(i=0; i<16; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferR[i+32]);	
			}

				 
			SSD2828_W_Reg(SigMode,channel,0xBC,3); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB3); //GAMMA RED GROUP4
			for(i=0; i<2; i++)
			{     
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferR[i+48]);	
			}

			SSD2828_W_Reg(SigMode,channel,0xBC,17); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB4); //GAMMA GREEN GROUP1
			for(i=0; i<16; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferG[i]);	
			}
								 
			SSD2828_W_Reg(SigMode,channel,0xBC,17); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB5); //GAMMA GREEN GROUP2
			for(i=0; i<16; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferG[i+16]);	
			}
								 
			SSD2828_W_Reg(SigMode,channel,0xBC,17); 
			SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB6); //GAMMA GREEN GROUP3
			for(i=0; i<16; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferG[i+32]);	
			}
							 
			SSD2828_W_Reg(SigMode,channel,0xBC,3); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB7); //GAMMA GREEN GROUP3
			for(i=0; i<2; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferG[i+48]);	
			}
							 
			SSD2828_W_Reg(SigMode,channel,0xBC,17); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB8); //GAMMA BLUE GROUP1
			for(i=0; i<16; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferB[i]);	
			}
							 
			SSD2828_W_Reg(SigMode,channel,0xBC,17); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xB9); //GAMMA BLUE GROUP2
			for(i=0; i<16; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferB[i+16]);	
			}
								 
			SSD2828_W_Reg(SigMode,channel,0xBC,17); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xBA); //GAMMA BLUE GROUP3
			for(i=0; i<16; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferB[i+32]);	
			}
			 
			SSD2828_W_Reg(SigMode,channel,0xBC,3); 
		    SSD2828_W_Cmd(SigMode,channel,0xBF);	
			SSD2828_W_Data(SigMode,channel,0xBB); //GAMMA BLUE GROUP4
			for(i=0; i<2; i++)
			{
		       SSD2828_W_Data(SigMode,channel,vtdr_bufferB[i+48]);	
			}					 
//					++m;
			delay_ms(50); 
//		}
		 	
	SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
	
	buffer1[0] = 0x02;        
    buffer1[1] = 0xC4;
    buffer1[2] = 0x00;    
    SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(5); 
	
    buffer[0]=0x4D;	//VTDR6100
    buffer[1]=0x08;		
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);            //返回 写寄存器状态  ：ok   
}

void VTDR6100_UP_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[7];
    u8 i,j,k=0,temp;
	u8 m = 0x00;
					
	if((buffer[3]&0xFF)==0x00)
	{
		for( i = 0;i<16;i++)   //8个绑点
		   {	                                                                                  
			   vtdr_bufferR[i]=buffer[i+5];	    
		   }
			 
		for( i = 0;i<16;i++)   //8个绑点
		   {	                                                                                  
			  vtdr_bufferR[i+16]=buffer[i+21]; 
		   }
			
		for( i = 0;i<16;i++)   //8个绑点
		   {	                                                                                  
			   vtdr_bufferR[i+32]=buffer[i+37]; 
		   }
			 
		for( i = 0;i<2;i++)   //1个绑点
		   {	                                                                                  
			   vtdr_bufferR[i+48]=buffer[i+53];  
		   }
	}
		
	else if((buffer[3]&0xFF)==0x42)
	{
		for( i = 0;i<16;i++)   //8个绑点
		{	                                                                                  
			  vtdr_bufferG[i]=buffer[i+5];
		} 
			 
		for( i = 0;i<16;i++)   //8个绑点
		   {	                                                                                  
			  vtdr_bufferG[i+16]=buffer[i+21]; 
		   } 
			 
		for( i = 0;i<16;i++)   //8个绑点
		   {	                                                                                  
			   vtdr_bufferG[i+32]=buffer[i+37]; 
		   }
			 
		for( i = 0;i<2;i++)   //1个绑点
		   {	                                                                                  
			   vtdr_bufferG[i+48]=buffer[i+53]; 
		   }
		}
		
	else if((buffer[3]&0xFF)==0x81)
	{
		for( i = 0;i<16;i++)   //8个绑点
		   {	                                                                                  
			   vtdr_bufferB[i]=buffer[i+5];	
		   } 
			 
		for( i = 0;i<16;i++)   //8个绑点
		   {	                                                                                  
			  vtdr_bufferB[i+16]=buffer[i+21];  
		   }
			 
		for( i = 0;i<16;i++)   //8个绑点
		   {	                                                                                  
			   vtdr_bufferB[i+32]=buffer[i+37]; 
		   }
			 
		for( i = 0;i<2;i++)   //1个绑点
		   {	                                                                                  
			   vtdr_bufferB[i+48]=buffer[i+53];  
		   }
	}
    buffer[0]=0x4D;	//VTDR6100
    buffer[1]=0x0A;		
    buffer[4] = Uart_Error_None;
    buffer[5] = 0;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);            //返回 写寄存器状态  ：ok  
//    delay_ms(100); 
}

void VTDR6100_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
	u8 buffer1[7];
	u16 tmp;
	u8 i,j;
	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //返回个数设置	    
	delay_ms(2);
				 
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB0;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA RED GROUP1
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
			
	for(i=0;i<16;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferR[i]=tmp>>8;
		vtdr_bufferR[i+1]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //返回个数设置	    
	delay_ms(2);
	
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB1;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA RED GROUP2
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
			
	for(i=0;i<16;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferR[i+16]=tmp>>8;
		vtdr_bufferR[i+17]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //返回个数设置	    
	delay_ms(2);
				 
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB2;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA RED GROUP3
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
			
	for(i=0;i<16;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferR[i+32]=tmp>>8;
		vtdr_bufferR[i+33]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); //返回个数设置	    
	delay_ms(2);
	
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB3;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA RED GROUP4
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
	
	for(i=0;i<2;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferR[i+48]=tmp>>8;
		vtdr_bufferR[i+49]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //返回个数设置	    
	delay_ms(2);
				 
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB4;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA GREEN GROUP1
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
			
	for(i=0;i<16;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferG[i]=tmp>>8;
		vtdr_bufferG[i+1]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //返回个数设置	    
	delay_ms(2);
	
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB5;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA GREEN GROUP2
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
	
	for(i=0;i<16;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferG[i+16]=tmp>>8;
		vtdr_bufferG[i+17]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //返回个数设置	    
	delay_ms(2);
				 
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB6;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA GREEN GROUP3
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
	
	for(i=0;i<16;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferG[i+32]=tmp>>8;
		vtdr_bufferG[i+33]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); //返回个数设置	    
	delay_ms(2);
				 
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB7;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA GREEN GROUP4
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
			
	for(i=0;i<2;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferG[i+48]=tmp>>8;
		vtdr_bufferG[i+49]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //返回个数设置	    
	delay_ms(2);
				 
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB8;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA BLUE GROUP1
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
			
	for(i=0;i<16;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferB[i]=tmp>>8;
		vtdr_bufferB[i+1]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //返回个数设置	    
	delay_ms(2);
	
	buffer1[0] = 0x02;        
	buffer1[1] = 0xB9;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA BLUE GROUP2
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
	
	for(i=0;i<16;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferB[i+16]=tmp>>8;
		vtdr_bufferB[i+17]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0010); //返回个数设置	    
	delay_ms(2);
	
	buffer1[0] = 0x02;        
	buffer1[1] = 0xBA;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA BLUE GROUP3
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
			
	for(i=0;i<16;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferB[i+32]=tmp>>8;
		vtdr_bufferB[i+33]=tmp;
		delay_ms(5);
		i++;			
	}
			
	SSD2828_W_Reg(SigMode,channel,0xC1,0x0002); //返回个数设置	    
	delay_ms(2);
				 
	buffer1[0] = 0x02;        
	buffer1[1] = 0xBB;          
	SSD2828_W_Array(SigMode,channel,buffer1,0);		 
	delay_ms(2);                 //GAMMA BLUE GROUP3
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
	SSD2828_W_Cmd(SigMode,channel,0xFF);
	delay_ms(2);
	
	for(i=0;i<2;i++)
	{
		SSD2828_W_Cmd(SigMode,channel,0xFA);		
		tmp=SPI3_Read_u16_Data(channel);
		vtdr_bufferB[i+48]=tmp>>8;
		vtdr_bufferB[i+49]=tmp;
		delay_ms(5);
		i++;			
	}
		
	if((buffer[3]&0xff)==0x00)       //GAMMA RED
	{     
		for(j=0;j<50;j++)
		{
			buffer[4+j]=vtdr_bufferR[j];	  	
		}               			   
	}
	else if((buffer[3]&0xff)==0x42)  //GAMMA GREEN
	{	
	    for(j=0;j<50;j++)
		{
				buffer[4+j]=vtdr_bufferG[j];	  		
		}              
	}
	else if((buffer[3]&0xff)==0x81)   //GAMMA BLUE
	{		
		for(j=0;j<50;j++)
		{
				buffer[4+j]=vtdr_bufferB[j];								
		}  
	} 
					
    buffer[0]=0x4D;	//VTDR6100
    buffer[1]=0x09;
		delay_ms(15);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);          //返回 读取的 Gamma数据  ：ok  		

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
}

void VTDR6100_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[10];
	u8 mtp_flag;
	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
	delay_ms(15); 
    buffer1[0] = 0x01;
    buffer1[1] = 0x28;                    
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
	delay_ms(10); 
	
	buffer1[0] = 0x03;
    buffer1[1] = 0xF0; 
	buffer1[2] = 0xAA;
	buffer1[3] = 0x12;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
	delay_ms(1); 
	
	buffer1[0] = 0x02;
    buffer1[1] = 0xCA; 
	buffer1[2] = 0x01;
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
	delay_ms(1); 
	
	if(buffer[1]==0x0D)  //otp Gamma
	 {
		buffer[1] = 0x0D;
	
		buffer1[0] = 0x05;
		buffer1[1] = 0xE6;
		buffer1[2] = 0x00;  
		buffer1[3] = 0x00;
		buffer1[4] = 0x80;  
		buffer1[5] = 0x00;  //OTP烧录项
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
	 else if(buffer[1]==0x0E)  //otp ALL
	 {
		buffer[1] = 0x0E;
		 
		buffer1[0] = 0x04;
		buffer1[1] = 0xE6;
		buffer1[2] = 0x9F;  
		buffer1[3] = 0x17;
		buffer1[4] = 0x80;  
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
	  else if(buffer[1]==0x0F)  //otp PAGE 0
	 {
		buffer[1] = 0x0F;
		 
		buffer1[0] = 0x04;
		buffer1[1] = 0xE6;
		buffer1[2] = 0xB1;  
		buffer1[3] = 0x00;
		buffer1[4] = 0x00;  
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
	 else if(buffer[1]==0x1B)  //otp PAGE 1
	 {
		buffer[1] = 0x1B;
		 
		buffer1[0] = 0x04;
		buffer1[1] = 0xE6;
		buffer1[2] = 0x02;  
		buffer1[3] = 0x00;
		buffer1[4] = 0x00;  
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
	 else if(buffer[1]==0x1C)  //otp PAGE 2
	 {
		buffer[1] = 0x1C;
		 
		buffer1[0] = 0x04;
		buffer1[1] = 0xE6;
		buffer1[2] = 0x04;  
		buffer1[3] = 0x00;
		buffer1[4] = 0x00;  
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
	 else if(buffer[1]==0x1D)  //otp PAGE 5
	 {
		buffer[1] = 0x1D;
		 
		buffer1[0] = 0x04;
		buffer1[1] = 0xE6;
		buffer1[2] = 0x00;  
		buffer1[3] = 0x02;
		buffer1[4] = 0x00;  
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	 }
	else if(buffer[1]==0x1A)  //otp PAGE 4
	{
		buffer[1] = 0x1A;
		 
		buffer1[0] = 0x04;
		buffer1[1] = 0xE6;
		buffer1[2] = 0x00;  
		buffer1[3] = 0x01;
		buffer1[4] = 0x00;  
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	}
	else if(buffer[1]==0x1E)  //otp PAGE 7
	{
		buffer[1] = 0x1E;
		 
		buffer1[0] = 0x04;
		buffer1[1] = 0xE6;
		buffer1[2] = 0x00;  
		buffer1[3] = 0x08;
		buffer1[4] = 0x00;  
		SSD2828_W_Array(SigMode,channel,buffer1,0); 
		delay_ms(10); 
	}
	 
    buffer1[0] = 0x04;
    buffer1[1] = 0xE7;
    buffer1[2] = 0x88;    
	buffer1[3] = 0x55;    
	buffer1[4] = 0x11;    
	SSD2828_W_Array(SigMode,channel,buffer1,0);	
	delay_ms(3000); 	
	
	buffer1[0] = 0x05;
    buffer1[1] = 0xE6;
    buffer1[2] = 0x00;  
	buffer1[3] = 0x00;
	buffer1[4] = 0x00;  
	buffer1[5] = 0x00;  //OTP烧录项
	SSD2828_W_Array(SigMode,channel,buffer1,0); 
	delay_ms(10); 

	
	buffer1[0] = 0x02;
	buffer1[1] = 0xCA;
	buffer1[2] = 0x02;  //
	SSD2828_W_Array(SigMode,channel,buffer1,0);     		 
	delay_ms(10);
//		buffer[1] = 0x0E;
	buffer[2] = 0x02;
	buffer[3] = Uart_Error_None;   //返回 OTP Gamma数据  ：OK
	buffer[4] = 0;
	STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);         
}


void VTDR6100_Read_OTP_Times(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u16 tmp;
    buffer[1]=0x03;
    SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080);                               
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0001); //return package size                                
    SSD2828_W_Reg(SigMode,channel,0xC0,0x0001);
    buffer[0] = 0x01;
    buffer[1] = 0xD8;                        
    SSD2828_W_Array(SigMode,channel,buffer,0);
    delay_ms(5);  
	SSD2828_W_Reg(SigMode,channel,0xD4,0x00FA);
    SSD2828_W_Cmd(SigMode,channel,0xFF);
	
	SSD2828_W_Cmd(SigMode,channel,0xFA);		
	buffer[4]=SPI3_Read_u8_Data(channel);
	delay_ms(5);

	buffer[0]=0x2D;	
	buffer[1]=0x0B;
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
	
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 读取的 寄存器数据  ：ok 
}

void ProcessForIc4D(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)
{	
	switch (USB_Rx_Buffer[1]&0xff)
	{            
		case 0x01:                                      //写寄存器
			VTDR6100_Write_Register(pdev,CDC_IN_EP,SigMode ,channel,USB_Rx_Buffer,LP_B7_Data,HS_B7_Data);break;	
		case 0x02:                                      //读寄存器
			VTDR6100_Read_Register(pdev,CDC_IN_EP,SigMode ,channel,USB_Rx_Buffer,LP_B7_Data,HS_B7_Data);break;	
		case 0x03:                                      //写寄存器
			VTDR6100_Write_51Register(pdev,CDC_IN_EP,SigMode ,channel,USB_Rx_Buffer,LP_B7_Data,HS_B7_Data);break;															
		case 0x08:                                      //写Gamma数据    
			VTDR6100_Write_Gamma(pdev,CDC_IN_EP,SigMode ,channel,USB_Rx_Buffer,LP_B7_Data,HS_B7_Data);break;
		case 0x09:                                      //读Gamma数据
			VTDR6100_Read_Gamma(pdev,CDC_IN_EP,SigMode ,channel,USB_Rx_Buffer,LP_B7_Data,HS_B7_Data);break; 
		//case 0x0A://gamma otp
									case 0x0D://OTP Gamma
		case 0x0E://OTP ALL
		case 0x0F://OTP P0 	
									case 0x1A://OTP P4
		case 0x1B://OTP P1
		case 0x1C://OTP P2 
									case 0x1D://OTP P5
		case 0x1E://OTP P7
			VTDR6100_Gamma_OTP_Start(pdev,CDC_IN_EP,OLED.SigMode ,channel,USB_Rx_Buffer,LP_B7_Data,HS_B7_Data);  break;
		case 0x0B://OTP times
			VTDR6100_Read_OTP_Times(pdev,CDC_IN_EP,OLED.SigMode ,channel,USB_Rx_Buffer,LP_B7_Data,HS_B7_Data);  break;	
									case 0x0A:                                      //写Gamma数据    
			VTDR6100_UP_Gamma(pdev,CDC_IN_EP,OLED.SigMode ,channel,USB_Rx_Buffer,LP_B7_Data,HS_B7_Data);break;
		default:	break;	
	}
}
	




/*************************************************************************************

*/
