#include "debug.h"
#include "AllDDIC.h"

/********************************************************************************************
********************************************************************************************
    HX83201A  写寄存器 读寄存器  写Gamma 读Gamma OTP烧录
********************************************************************************************
********************************************************************************************/

void SSD2828_W_Array_Demura			(u8 SigMode ,u8 channel,u8 para[],u8 offerset);
void HX83201A_Write_Register		 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data);
void HX83201A_Write_Register_Demura	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void HX83201A_Read_Register		 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void HX83201A_Write_Gamma			 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void HX83201A_Gamma_OTP_Start		 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);
void HX83201A_DLL_Calculation		 	(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);	
void HX83201A_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data);



void HX83201A_Write_Register_Demura(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
//   if(SigMode != CMD_Mode)
//        SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
//		else
//				SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
//    delay_ms(5);
//    SSD2828_W_Array(SigMode,channel,buffer,2);
//    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
//    delay_ms(5);
//    if(buffer[0]==RM67195)
//    {
//        SSD2828_W_Reg(SigMode,channel,0xBF,0xAAFB); 
//    }
//    buffer[4] = Uart_Error_None;
//    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写寄存器状态  ：ok
}
void HX83201A_Write_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    //SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data&0xFBFF)|0x0400); 
		SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);		
    delay_ms(5);
	
//	if( (buffer[1]&0xc0)>0)
//		SSD2828_W_Array_Demura(SigMode,channel,buffer,2);
//    else
		SSD2828_W_Array(SigMode,channel,buffer,2);

    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写寄存器状态  ：ok
}
void HX83201A_Read_Register(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[2];
		u8 i=0;
	
	
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    delay_ms(5);
    SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);                                
    buffer1[0] = 0x01;
    buffer1[1] = buffer[3];
    SSD2828_W_Array(SigMode,channel,buffer1,0); 
    delay_ms(25);
//    while((i<5)&&((SSD2828_R_Reg(SigMode,channel,0xc6)&0x01)!=1))
//		{
//			i++;
//			if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
//        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
//			else
//					SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
//			delay_ms(5);
//			SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);                                
//			buffer1[0] = 0x01;
//			buffer1[1] = buffer[3];
//			SSD2828_W_Array(SigMode,channel,buffer1,0); 
//			delay_ms(20);
//		}
//		buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFa);
		buffer[4]=SSD2828_R_Reg(SigMode,channel,0xFF);
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 读取的 寄存器数据  ：ok  
}
void HX83201A_Write_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
		u8 buffer1[3];
		u8 i=0,j=0,k=0,l=0,temp=0;
			int RED_START 	= 0+3;
			int GREEN_START	= 40+3;
			int BLUE_START 	= 80+3;
		
	
	//	SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data);
    SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0010)&0XFFFD);
		delay_ms(5);
    buffer1[0] = 0x02;
    buffer1[1] = 0xB0;
		buffer1[2] = 0x06;  //PAGE 6
	SSD2828_W_Array(SigMode,channel,buffer1,0);delay_ms(5);
	for(i=0;i<=0x25;i++)/////write red gamma
	{
		buffer1[0] = 0x02;
		buffer1[1] = 0xB1+i;
		buffer1[2] = buffer[RED_START+i];
		SSD2828_W_Array(SigMode,channel,buffer1,0);delay_ms(5);
	}
		
	buffer1[0] = 0x02;
    buffer1[1] = 0xB0;
	buffer1[2] = 0x07;  //PAGE 7
	SSD2828_W_Array(SigMode,channel,buffer1,0);	delay_ms(5);
		for(i=0;i<=0x25;i++)/////write green gamma
		{
		buffer1[0] = 0x02;
		buffer1[1] = 0xB1+i;
		buffer1[2] = buffer[GREEN_START+i];
		SSD2828_W_Array(SigMode,channel,buffer1,0);delay_ms(5);
		}
	
	buffer1[0] = 0x02;
    buffer1[1] = 0xB0;
	buffer1[2] = 0x08;  //PAGE 8
	SSD2828_W_Array(SigMode,channel,buffer1,0);	delay_ms(5);
	for(i=0;i<=0x25;i++)/////write blue gamma
	{
		buffer1[0] = 0x02;
		buffer1[1] = 0xB1+i;
		buffer1[2] = buffer[BLUE_START+i];
		SSD2828_W_Array(SigMode,channel,buffer1,0);delay_ms(5);
	}
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    buffer[4] = Uart_Error_None;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写Gamma状态  ：ok  
}

void HX83201A_Read_Gamma(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
    u8 buffer1[3];
		u32 i=0;
		int RED_START 	= 0+3;
		int GREEN_START	= 40+3;
		int BLUE_START 	= 80+3;
	
	SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);		
    delay_ms(5);
	buffer1[0] = 0x02;
			buffer1[1] = 0xb0;
			buffer1[2] = 0x06;
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(25);
	
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    delay_ms(5);
//    SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);                                
//    buffer1[0] = 0x01;
//    buffer1[1] = buffer[3];
//    SSD2828_W_Array(SigMode,channel,buffer1,0); 
//    delay_ms(25);
//    buffer[3]=SSD2828_R_Reg(SigMode,channel,0xFF);
		
			
//			SSD2828_R_Reg(SigMode,channel,0xFF);
		for(i=0;i<=0x25;i++)
		{
//			delay_ms(5);
			SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);
			
			buffer1[0] = 0x01;
			buffer1[1] = 0xb1+i;
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(35);
			buffer[RED_START+i]=SSD2828_R_Reg(SigMode,channel,0xFF);
		}
		
		SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);		
    delay_ms(5);
	buffer1[0] = 0x02;
			buffer1[1] = 0xb0;
			buffer1[2] = 0x07;
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(25);
	
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    delay_ms(5);
		
		for(i=0;i<=0x25;i++)
		{
//			delay_ms(5);
			SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);
			
			buffer1[0] = 0x01;
			buffer1[1] = 0xb1+i;
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(35);
			buffer[GREEN_START+i]=SSD2828_R_Reg(SigMode,channel,0xFF);
		}
		
		///LP tx mode 
		SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);		
    delay_ms(5);
			buffer1[0] = 0x02;
			buffer1[1] = 0xb0;
			buffer1[2] = 0x08;
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(25);
	
		////read mode
    if(SigMode == Mipi_Mode)  //RGB+SPI= MIPI
        SSD2828_W_Reg(SigMode,channel,0xB7,((HS_B7_Data|0x0010)&0XFFFD)|0x0080);      
    else
        SSD2828_W_Reg(SigMode,channel,0xB7,LP_B7_Data|0x0080); 
    delay_ms(5);
		
		for(i=0;i<=0x25;i++)
		{
//			delay_ms(5);
			SSD2828_W_Reg(SigMode,channel,0xC1,0x0002);
			
			buffer1[0] = 0x01;
			buffer1[1] = 0xb1+i;
			SSD2828_W_Array(SigMode,channel,buffer1,0); 
			delay_ms(35);
			buffer[BLUE_START+i]=SSD2828_R_Reg(SigMode,channel,0xFF);
		}
		
		////hs mode
    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
    delay_ms(5);
		buffer1[0] = 0x51;
    buffer1[1] = 0x05;
    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 读取的 寄存器数据  ：ok  
}

void HX83201A_Gamma_OTP_Start(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
//	u8 buffer1[6];
//	SSD2828_W_Reg(SigMode,channel,0xB7,(LP_B7_Data&0xFBFF)|0x0400); 	
//	delay_ms(15);
//    buffer1[0] = 0x02;
//    buffer1[1] = 0xB0; 
//    buffer1[2] = 0x04;
//    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//# Unlock MCP
//    buffer1[0] = 0x01;
//    buffer1[1] = 0x28; 
//    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//#  Display off
//    buffer1[0] = 0x01;
//    buffer1[1] = 0x10; 
//    SSD2828_W_Array(SigMode,channel,buffer1,0);   //#  Enter_sleep_mode
//		delay_ms(234);
//    buffer1[0] = 0x02;
//    buffer1[1] = 0xB0; 
//    buffer1[2] = 0x84;
//    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//# Bank 1
//    buffer1[0] = 0x02;
//    buffer1[1] = 0xE6; 
//    buffer1[2] = 0x81;
//    SSD2828_W_Array(SigMode,channel,buffer1,0);	  //# NVM load on after exit_Sleep mode
//    buffer1[0] = 0x02;
//    buffer1[1] = 0xB0; 
//    buffer1[2] = 0x04;
//    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//# Bank 0		
//    buffer1[0] = 0x02;
//    buffer1[1] = 0xE3; 
//    buffer1[2] = 0xFF;
//    SSD2828_W_Array(SigMode,channel,buffer1,0); 	//# NVM load enable			
//    buffer1[0] = 0x05;
//    buffer1[1] = 0xE0; 
//    buffer1[2] = 0x61;
//    buffer1[3] = 0x00; 
//    buffer1[4] = 0x00;		
//    buffer1[5] = 0x01;			
//    SSD2828_W_Array(SigMode,channel,buffer1,0); //# NVM write Start
//		delay_ms(1234);	
//    buffer1[0] = 0x02;
//    buffer1[1] = 0xE0; 
//    buffer1[2] = 0x11;
//    SSD2828_W_Array(SigMode,channel,buffer1,0); 	
//		delay_ms(1234);	
//    SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
//    delay_ms(5);
//    buffer[4] = Uart_Error_None;
//    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写Gamma状态  ：ok 		
}

void HX83201A_DLL_Calculation(USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8* buffer,u16 LP_B7_Data,u16 HS_B7_Data)
{
//	u8 buffer1[3];
//	SSD2828_W_Reg(SigMode,channel,0xB7,(HS_B7_Data|0x0410)&0XFFFD);														
//	delay_ms(5);		
//	
//    buffer1[0] = 0x02;//group0 inter
//    buffer1[1] = 0xB0; 
//    buffer1[2] = 0x00;
//    SSD2828_W_Array(SigMode,channel,buffer1,0); 	
//	delay_ms(5);
//	SSD2828_W_Array(SigMode,channel,buffer,2);
//	delay_ms(5);

//	SSD2828_W_Reg(SigMode,channel,0xB7,HS_B7_Data);
//    delay_ms(5);
//    buffer[4] = Uart_Error_None;
//    STM2PC_RM671xx(pdev,CDC_IN_EP,buffer,buffer[2]+3);           //返回 写Gamma状态  ：ok  	;
}

void ProcessForIc51(  USB_OTG_CORE_HANDLE *pdev,uint8_t  ep_addr,u8 SigMode ,u8 channel,u8 buffer[],u16 LP_B7_Data,u16 HS_B7_Data)
{	
	//void *pdev;
	switch(buffer[1]&0x0f)
	{
		case 0x01:                                      //写寄存器
				HX83201A_Write_Register(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		case 0x02:                                      //读寄存器
				HX83201A_Read_Register(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		case 0x03:                                      //写Gamma
				HX83201A_Write_Gamma(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data);
				break;
		case 0x04:                                      //读Gamma
				HX83201A_Gamma_OTP_Start(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data); 
				break;
		case 0x05: 
				HX83201A_DLL_Calculation		(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data); 
				break;
		case 0x0f:
				//HX83201A_Write_Register_Demura(pdev,ep_addr,SigMode,channel,buffer,LP_B7_Data,HS_B7_Data);
				//Demura: 0x30 0x0f add3 add2 add1 add0 //0xf4(spiflashbufferaddr)
//				DemuraFlagStart();	
				break;
		default:
			break;
	}			
}
	




/*************************************************************************************

*/
